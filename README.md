# ChoiceConnect

**The Team:**  
Karthik Nemmani  
Shreyansh Dixit  
Ethan Benson  
Tarun Mohan  
Shrey Gupta  

**Project Name:**  
ChoiceConnect  

**Canvas Group Number:**
02

**Project Description:**  
With the recent overturning of the Roe v. Wade case, there have been many concerns relating to women's reproductive rights, namely the outlawing of abortions in several states. At ChoiceConnect, we will serve an accessible platform dedicated to providing crucial resources to ultimately ensure safe reproduction and medical care among American women.  
  
**Website URL:**  
https://www.choiceconnect.me/  

**API URL:**
https://api.choiceconnect.me/  

**API Docs Link:**  
https://documenter.getpostman.com/view/32889098/2sA2r6WPJ9

**PechaKucha Video:**
https://www.youtube.com/watch?v=GhhAi0aewoU

**Gitlab SHA:**
44f0e96d37156e7fb69b704b1736efaff9b5ec21

**Gitlab Pipeline Link:** 
https://gitlab.com/karthiknemmani/cs373-group-2/-/pipelines

**Estimated and Actual Work Times:**
| Name | GitLab ID | Est. Completion Time (hrs) | Actual Completion Time (hrs) |
|---|---|---|---|
| Tarun Mohan | [@tarun.mohan](https://gitlab.com/tarun.mohan) | 9 | 15 |
| Shreyansh Dixit | [@shreyanshd43](https://gitlab.com/shreyanshd43) | 10 | 14 |
| Karthik Nemmani | [@karthiknemmani](https://gitlab.com/karthiknemmani) | 8 | 14 |
| Ethan Benson | [@qquiche](https://gitlab.com/qquiche) | 10 | 12 |
| Shrey Gupta | [@https://gitlab.com/shreygupta1](https://gitlab.com/shreygupta1) | 11 | 8 |



**Phase 1 Leader:**  
Karthik was our leader for Phase 1. He did solid research on Docker and Bootstrap and was able to configure the website to meet future specifications. Also, he was very diligent in the frontend and ensured everyone could meet at good times. 

**Phase 2 Leader:**
Shrey was our leader for Phase 2. He had to most experience in backend and took the initiative in starting backend hosting early, even back in Phase 1. He also took the lead when webscraping datsources and setting up our datbases and API. He allowed the rest of us to integrate everything into the frontend in a timely manner.

**Phase 3 Leader:**
Ethan was our leader for Phase 3. He took the initiative this phase, working on each aspect of this phase to make sure that was properly tended to. He lead the filtering and sorting, researching how best to implement it alongside the searches required for this phase. He also helped the rest of us on all of our components so that it integrates smoothly.

**Phase 4 Leader:**
Shreyansh was our leader for Phase 4. He was very diligent in his work on the visualization aspects of the website. Additionally, he made sure that our team stayed on task and completed all of the different aspects of this phase. Finally, he took charge on creating the structure for the PechaKucha presentation and editing the video so it was seamless.

**URLs of Data Sources:**
- [Abortion Policy API (RESTful API)](https://www.abortionpolicyapi.com/)
- [Yelp Fusion API (RESTful API)](https://fusion.yelp.com/ )
- [Google Maps API](https://developers.google.com/maps/apis-by-platform)
- [ScenarioMAB Closest Facilities for Each Closing Facility](https://about-the-abortion-access-dashboard-analysis-1.hub.arcgis.com/maps/38ea3d6beffa46d989aa8bd30cb2149a/explore)
- [Planned Parenthood Events](https://act.plannedparenthoodaction.org/events)

**Models:**
- Community Activities (~350 instances)
    - Name
    - Zip code
    - City
    - State
    - Date Range
- Legislation Information (~50 instances)
    - State
    - Banned after weeks since (LMP)
    - Exception life (Allowed to save parent’s life)
    - Exception health (Allowed to protect parent’s health)
    - Exception fetal (Allowed when Fetal has lethal anomaly)
    - Exception rape or incest (Allowed in cases of Rape/Incest)
- Cities (~144 instances)
    - Number of Facilities
    - State
    - Females of Reproductive Age
    - Average Travel Distance
    - Average Travel Time

**Connection Between Models/Instances:**
- Community Activities - Legislation
    - Based on the US state of the event, we link to that state’s legislation policy
- Community Activities - Cities
    - Links to the closest state for legislation
- Legislation Information - Community Activities
    - Based on the location where said legislation is passed, link to community activities in that state
- Legislation Information - Cities
    - Link to a list of cities for each state on their legislation page
- Cities - Community Activities
    - Links to closest local community activities in state
- Cities - Legislation
    - Each clinic/practice page will link to the legislation information on abortion for the state it's located in.

**Media per Model:**
- Cities
    - Use images of establishments, map to each establishment
- Legislation Information
    - State icon images, text describing specific legislation
- Community Activity
    - Text describing location and time, map to said location

**Questions our project will answer:**
- What legislation on abortion does my state have?
- Where are nearby places that can provide adequate reproductive care (abortion, birth control, etc)?
- What are some events that I can attend to get involved in supporting women’s rights?

Comments:
Acknowledgement to Veteran Aid for their NavBar component which we adopted.
Acknowledgment to Native Aid Network for their About page framework which we built on top of.
Acknowledgment to Syrian Refugee Crisis for Gitlab Pipeline example

