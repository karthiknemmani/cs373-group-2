.PHONY: start docker frontend-image && start docker backend-image

start:
	cd frontend && npm start

docker:
	docker-compose build
	docker-compose up

docker-image:
	cd frontend && \
	docker build -t frontend-image . && \
	cd ../backend && \
	docker build -t backend-image . && \
	cd ..