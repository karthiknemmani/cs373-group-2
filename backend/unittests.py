import unittest
from application import application

class Tests(unittest.TestCase):
    def setUp(self):
        self.client = application.test_client()
        self.client.testing = True
        
    def test_invalid_route(self):
        with self.client:
            response = self.client.get('/invalid')
            self.assertEqual(response.status_code, 404)
            
    def test_about(self):
        with self.client:
            response = self.client.get('/about')
            self.assertEqual(response.status_code, 200)
            
    def test_legislation(self):
        with self.client:
            response = self.client.get('/legislation')
            self.assertEqual(response.status_code, 200)
    
    def test_activities(self):
        with self.client:
            response = self.client.get('/activities')
            self.assertEqual(response.status_code, 200)
            
    def test_cities(self):
        with self.client:
            response = self.client.get('/clinics')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_legislation_id(self):
        with self.client:
            response = self.client.get('/legislation?id=3')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_activities_id(self):
        with self.client:
            response = self.client.get('/activities?id=3')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_cities_id(self):
        with self.client:
            response = self.client.get('/clinics?id=3')
            self.assertEqual(response.status_code, 200)
            
    def test_invalid_legislation_id(self):
        with self.client:
            response = self.client.get('/legislation?id=1000')
            self.assertEqual(response.status_code, 500)
            
    def test_invalid_activities_id(self):
        with self.client:
            response = self.client.get('/activities?id=1000')
            self.assertEqual(response.status_code, 500)
            
    def test_invalid_cities_id(self):
        with self.client:
            response = self.client.get('/clinics?id=1000')
            self.assertEqual(response.status_code, 500)
            
    def test_valid_legislation(self):
        with self.client:
            response = self.client.get('/legislation?page=1&num_entries=10')
            self.assertEqual(response.status_code, 200)
            
    def test_valid_activities(self):
        with self.client:
            response = self.client.get('/activities?page=1&num_entries=10')
            self.assertEqual(response.status_code, 200)
            
    def test_valid_cities(self):
        with self.client:
            response = self.client.get('/clinics?page=1&num_entries=10')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_cities_sort(self):
        with self.client:
            response = self.client.get('/clinics?sort_by=num_facilities&ascending=True&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)

    def test_valid_activities_sort(self):
        with self.client:
            response = self.client.get('/activities?sort_by=zip&ascending=True')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_legislation_sort(self):
        with self.client:
            response = self.client.get('/legislation?sort_by=no_restrictions&ascending=True&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)

    def test_valid_cities_filter(self):
        with self.client:
            response = self.client.get('/clinics?filter_bycity=null&param=California&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_activities_filter(self):
        with self.client:
            response = self.client.get('/activities?filter_by=state&param=Texas&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)

    def test_valid_legislation_filter(self):
        with self.client:
            response = self.client.get('/legislation?filter_by=banned_after_weeks_since_LMP&param=12&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_cities_search(self):
        with self.client:
            response = self.client.get('/search?query=Akron&type=clinics&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)
    
    def test_valid_activities_search(self):
        with self.client:
            response = self.client.get('/search?query=Parent&type=activities&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)

    def test_valid_legislation_search(self):
        with self.client:
            response = self.client.get('search?query=Texas&type=legislation&page=1&num_entries=9')
            self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()

