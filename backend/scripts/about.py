import requests

def getContributions():
    token = 'glpat-5JHZSLsjwm9hEHU4Fyyw'
    projectId = '54659571'

    members = {
        "Shrey Gupta": {"commits": 0, "issues": 0, "alt_names": ["shreygupta04"]},
        "Shreyansh Dixit": {"commits": 0, "issues": 0, "alt_names": ["shreyanshd43"]},
        "Karthik Nemmani": {"commits": 0, "issues": 0, "alt_names": ["karthiknemmani"]},
        "Tarun Mohan": {"commits": 0, "issues": 0, "alt_names": ["tarun.mohan", "tarunmohan19"]},
        "Ethan Benson": {"commits": 0, "issues": 0, "alt_names": ["qquiche"]}
    }

    totals = {
        "commits": 0,
        "issues": 0
    }

    params = {
        "PRIVATE-TOKEN": token,
    }

    urls = [f'https://gitlab.com/api/v4/projects/{projectId}/repository/contributors', f'https://gitlab.com/api/v4/projects/{projectId}/issues?per_page=100']
    
    contributions = requests.get(urls[0], params=params).json()
    issues = requests.get(urls[1], params=params).json()

    def updateCommits(data):
        if data['name'] in members:
            members[data['name']]['commits'] = data['commits']
            totals["commits"] += data['commits']
        else:
            for member, memberInfo in members.items():
                if data['name'] in memberInfo['alt_names']:
                    memberInfo['commits'] = data['commits']
                    totals["commits"] += data['commits']
                    break

    for contribution in contributions:
        updateCommits(contribution)
    
    # print(members)
    def updateIssues(data):
        if data['closed_by']['name'] in members:
            members[data['closed_by']['name']]['issues'] += 1
            totals["issues"] += 1
        else:
            for member, memberInfo in members.items():
                if data['closed_by']['name'] in memberInfo['alt_names']:
                    memberInfo['issues'] += 1
                    totals["issues"] += 1
                    break
    for issue in issues:
        if issue["closed_by"]:
            updateIssues(issue)
    

    return {
        "members": members,
        "totals": totals
    }
