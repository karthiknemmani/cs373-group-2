import requests


def getClinics(page, entries):
    url = 'https://services1.arcgis.com/hLJbHVT9ZrDIzK0I/arcgis/rest/services/ScenarioMAB_Surplus_in_Average_Service_Population/FeatureServer/106/query?outFields=*&where=1%3D1&f=geojson'
    
    clinics = requests.get(url).json().get('features')
    
    filtered_clinics = []
    
    if (page-1)*entries <= len(clinics):
        filtered_clinics = clinics[(page-1)*entries:page*entries] if page*entries <= len(clinics) else clinics[(page-1)*entries:]
    
    clinic_data = {}
    for clinic in filtered_clinics:
        props = clinic['properties']
        clinic_data[clinic['id']] = {
            'id': props['OBJECTID'],
            'city': props['Location_Name'].split(',')[0],
            'state_abbr': props['STATE_ABBREV'],
            'state': props['STATE_NAME'],
            'travel_time': int(props['mean_mean_total_traveltime']),
            'travel_distance': int(props['mean_mean_total_miles']),
            'total_females': props['sum_sum_females_age_15_44'],
            'num_facilities': props['Point_Count']
        }
    
    return clinic_data

def getClinicsById(id):
    url = 'https://services1.arcgis.com/hLJbHVT9ZrDIzK0I/arcgis/rest/services/ScenarioMAB_Surplus_in_Average_Service_Population/FeatureServer/106/query?outFields=*&where=1%3D1&f=geojson'
    
    clinics = requests.get(url).json().get('features')
    
    clinic_data = {}
    for clinic in clinics:
        props = clinic['properties']
        clinic_data[clinic['id']] = {
            'id': props['OBJECTID'],
            'city': props['Location_Name'].split(',')[0],
            'state_abbr': props['STATE_ABBREV'],
            'state': props['STATE_NAME'],
            'travel_time': props['mean_mean_total_traveltime'],
            'travel_distance': props['mean_mean_total_miles'],
            'total_females': props['sum_sum_females_age_15_44'],
            'num_facilities': props['Point_Count']
        }
    
    return clinic_data[id] if id in clinic_data else None

getClinics(1, 10)