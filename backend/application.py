from flask import request, url_for
from flask_cors import CORS, cross_origin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_,extract

from scripts.about import getContributions


from database import *



@application.route('/')
def home():
    html = f'''
        <div style="margin-left:1rem;">
            <h1>Welcome to Choice Connect API</h1>
            <h3><a href="{url_for('about')}">/about</a></h3>
            <h3><a href="{url_for('legislation')}">/legislation</a></h3>
            <h3><a href="{url_for('activities')}">/activities</a></h3>
            <h3><a href="{url_for('clinics')}">/clinics</a></h3>
            <h3><a href="{url_for('search')}">/search</a></h3>
        </div>
    '''
    return html


@application.route('/about', methods=['GET'])
@cross_origin()
def about():
    return getContributions()

@application.route('/legislation', methods=['GET'])
@cross_origin()
def legislation():
    filter_attribute = request.args.get('filter_by', type=str)
    filter_param = request.args.get('param', type=str)

    sort_attribute = request.args.get('sort_by', type=str)
    sort_ascending = request.args.get('ascending', type=str)

    page = request.args.get('page', type=int)
    num_entries = request.args.get('num_entries', type=int)

    id = request.args.get('id', type=int)

    if id:
        return getLegislationByID(id)
    
    if page and num_entries:
        if filter_attribute and filter_param:
            legislation = getLegislationByPagination(page, num_entries, getLegislationFilterable(filter_attribute, filter_param))
        elif sort_attribute and sort_ascending:
            legislation = getLegislationByPagination(page, num_entries, getLegislationSorted(sort_attribute, sort_ascending))
        else:
            legislation = getLegislationByPagination(page, num_entries)
    elif filter_attribute and filter_param:
        legislation = getLegislationFilterable(filter_attribute, filter_param)
    elif sort_attribute and sort_ascending:
        legislation = getLegislationSorted(sort_attribute, sort_ascending)
    else:
        legislation = getLegislation()
    
    return packageIntoJSON(legislation, Legislation)


@application.route('/activities', methods=['GET'])
@cross_origin()
def activities():
    filter_attribute = request.args.get('filter_by', type=str)
    filter_param = request.args.get('param', type=str)

    sort_attribute = request.args.get('sort_by', type=str)
    sort_ascending = request.args.get('ascending', type=str)

    page = request.args.get('page', type=int)
    num_entries = request.args.get('num_entries', type=int)

    id = request.args.get('id', type=int)

    if id:
        return getActivitiesByID(id)
    
    if page and num_entries:
        if filter_attribute and filter_param:
            activities = getActivitiesByPagination(page, num_entries, getActivitiesFilterable(filter_attribute, filter_param))
        elif sort_attribute and sort_ascending:
            activities = getActivitiesByPagination(page, num_entries, getActivitiesSorted(sort_attribute, sort_ascending))
        else:
            activities = getActivitiesByPagination(page, num_entries)
    elif filter_attribute and filter_param:
        activities = getActivitiesFilterable(filter_attribute, filter_param)
    elif sort_attribute and sort_ascending:
        activities = getActivitiesSorted(sort_attribute, sort_ascending)
    else:
        activities = getActivities()
    
    return packageIntoJSON(activities, Activity)

@application.route('/clinics', methods=['GET'])
@cross_origin()
def clinics():
    filter_attribute = request.args.get('filter_by', type=str)
    filter_param = request.args.get('param', type=str)

    sort_attribute = request.args.get('sort_by', type=str)
    sort_ascending = request.args.get('ascending', type=str)

    page = request.args.get('page', type=int)
    num_entries = request.args.get('num_entries', type=int)

    id = request.args.get('id', type=int)

    if id:
        return getClinicsByID(id)
    
    if page and num_entries:
        if filter_attribute and filter_param:
            clinics = getClinicsByPagination(page, num_entries, getClinicsFilterable(filter_attribute, filter_param))
        elif sort_attribute and sort_ascending:
            clinics = getClinicsByPagination(page, num_entries, getClinicsSorted(sort_attribute, sort_ascending))
        else:
            clinics = getClinicsByPagination(page, num_entries)
    elif filter_attribute and filter_param:
        clinics = getClinicsFilterable(filter_attribute, filter_param)
    elif sort_attribute and sort_ascending:
        clinics = getClinicsSorted(sort_attribute, sort_ascending)
    else:
        clinics = getClinics()
    
    return packageIntoJSON(clinics, Clinic)

counts = {
    "legislation": {},
    "activities": {},
    "clinics": {}
}

@application.route('/search', methods=['GET'])
@cross_origin()
def search():
    search_query = request.args.get('query', type=str)
    if not search_query:
        return {"error": "No search query provided"}
    search_type = request.args.get('type', type=str)
    page = request.args.get('page', type=int)
    num_entries = request.args.get('num_entries', type=int)
    
    search_query = set(search_query.split())

    unique_search_results = {
        "legislation": {},
        "activities": {},
        "clinics": {}
    }
    
    for word in search_query:
        if search_type == 'legislation':
            results = packageIntoJSON(searchLegislation(word), Legislation)
            if page and num_entries:
                results['legislation'] = results['legislation'][(page-1)*num_entries:page*num_entries]
            for result in results['legislation']:
                unique_search_results["legislation"][result['id']] = result

        elif search_type == 'activities':
            results = packageIntoJSON(searchActivities(word), Activity)
            if page and num_entries:
                results['activities'] = results['activities'][(page-1)*num_entries:page*num_entries]
            for result in results['activities']:
                print(result)
                unique_search_results["activities"][result['id']] = result

        elif search_type == 'clinics':
            results = packageIntoJSON(searchClinics(word), Clinic)
            if page and num_entries:
                results['clinics'] = results['clinics'][(page-1)*num_entries:page*num_entries]
            for result in results['clinics']:
                unique_search_results["clinics"][result['id']] = result
        else:
            return {"error": "Invalid search type"}
    
    for model in unique_search_results:
        items_list = list(unique_search_results[model].items())
        sorted_items_by_count = sorted(items_list, key=lambda item: counts.get(model, {}).get(item[0], 0), reverse=True)
        unique_search_results[model] = {item[0]: item[1] for item in sorted_items_by_count}

    final_results = {model: list(items.values()) for model, items in unique_search_results.items() if items}
    final_results["count"] = sum(len(items) for items in final_results.values())
    return final_results


def searchLegislation(search_query):
    all_leg_string = "Number of Weeks Before Abortion Banned:" + "Exception for Danger to Mother's Life:" + "Exception for Lethal Anomaly in Fetus:" + "Exception for Rape/Incest:"
    search_condition = or_(
        Legislation.state.ilike(f"%{search_query}%"),
        # Legislation.date.ilike(f"%{search_query}%"),
        Legislation.banned_after_weeks_since_LMP.ilike(f"%{search_query}%"),
        Legislation.exception_fetal.ilike(f"%{search_query}%"),
        Legislation.exception_health.ilike(f"%{search_query}%"),
        Legislation.exception_life.ilike(f"%{search_query}%"),
        Legislation.exception_rape_or_incest.ilike(f"%{search_query}%"),
        Legislation.no_restrictions.ilike(f"%{search_query}%"),
    )
    if search_query in all_leg_string:
        res = Legislation.query.all()
    else:
        res = Legislation.query.filter(search_condition).all()
    for result in res:
        if result.id in counts["legislation"]:
            counts["legislation"][result.id] += 1
        else:
            counts["legislation"][result.id] = 1
    return res

def searchActivities(search_query):
    search_conditions = [
        Activity.title.ilike(f"%{search_query}%"),
        Activity.time.ilike(f"%{search_query}%"),
        Activity.state.ilike(f"%{search_query}%"),
        Activity.venue.ilike(f"%{search_query}%"),
        Activity.address.ilike(f"%{search_query}%"),
        Activity.zip.ilike(f"%{search_query}%"),
    ]

    # Attempt to convert the search query to a year or month for comparison
    try:
        year_query = int(search_query)
        # If the search query is a valid year (4 digits), add a condition to match the year
        if 1900 <= year_query <= 2100:
            search_conditions.append(extract('year', Activity.date) == year_query)
    except ValueError:
        # Not a valid integer, ignore year search
        pass

    try:
        month_query = int(search_query)
        # If the search query is a valid month (1-12), add a condition to match the month
        if 1 <= month_query <= 12:
            search_conditions.append(extract('month', Activity.date) == month_query)
    except ValueError:
        # Not a valid integer, ignore month search
        pass
    
    res = Activity.query.filter(or_(*search_conditions)).all()
    for result in res:
        if result.id in counts["activities"]:
            counts["activities"][result.id] += 1
        else:
            counts["activities"][result.id] = 1
    return res

def searchClinics(search_query):
    all_clinics_string = "Number Of Facilities:" + "Women of Reproductive Age:" + "Average Distance/Time:"
    search_condition = or_(
        Clinic.city.ilike(f"%{search_query}%"),
        Clinic.state.ilike(f"%{search_query}%"),
        Clinic.travel_distance.ilike(f"%{search_query}%"),
        Clinic.travel_time.ilike(f"%{search_query}%"),
        Clinic.total_females.ilike(f"%{search_query}%"),
    )
    if search_query in all_clinics_string:
        res = Clinic.query.all()
    else:
        res = Clinic.query.filter(search_condition).all()
    for result in res:
        if result.id in counts["clinics"]:
            counts["clinics"][result.id] += 1
        else:
            counts["clinics"][result.id] = 1
    return res


def packageIntoJSON(data, dataType):
    if dataType == Legislation:
        return {
            "count": len(data),
            "legislation": [legislation.to_dict() for legislation in data]
        }
    elif dataType == Activity:
        return {
            "count": len(data),
            "activities": [activity.to_dict() for activity in data]
        }
    else:
        return {
            "count": len(data),
            "clinics": [clinic.to_dict() for clinic in data]
        }

if __name__ == '__main__':
    application.run(port=8000, debug=True)
