from flask import Flask, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
from config import Config
from dotenv import load_dotenv

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from dateutil import parser

import requests
import os

from Flask_SQLalchemy_Whoosh.searcher import WhooshSearcher
from Flask_SQLalchemy_Whoosh.mixin import SearchableMixin

load_dotenv()

application = Flask(__name__)
application.config.from_object(Config)
CORS(application)
db = SQLAlchemy()
db.init_app(application)

STATE_LIST = [
    "Alabama", "Alaska", "Arizona", "Arkansas", "California",
    "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida",
    "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
    "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
    "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri",
    "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
    "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio",
    "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina",
    "South Dakota", "Tennessee", "Texas", "Utah", "Vermont",
    "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"
]

STATE_DICT = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
}

activity_legislation_association = db.Table('activity_legislation',
    db.Column('activity_id', db.Integer, db.ForeignKey('activity.id')),
    db.Column('legislation_id', db.Integer, db.ForeignKey('legislation.id'))
)

activity_clinics_association = db.Table('activity_clinics',
    db.Column('activity_id', db.Integer, db.ForeignKey('activity.id')),
    db.Column('clinics_id', db.Integer, db.ForeignKey('clinics.id'))
)

legislation_clinics_association = db.Table('legislation_clinics',
    db.Column('clinics_id', db.Integer, db.ForeignKey('clinics.id')),
    db.Column('legislation_id', db.Integer, db.ForeignKey('legislation.id'))
)

class Activity(db.Model):
    __tablename__ = 'activity'
    __searchable__ = ['title', 'date', 'time', 'state', 'venue', 'address', 'zip']

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500))
    date = db.Column(db.String(20))
    time = db.Column(db.String(20))
    state = db.Column(db.String(20))
    venue = db.Column(db.String(500))
    address = db.Column(db.String(500))
    zip = db.Column(db.String(5))
    url = db.Column(db.String(500))

    legislations = db.relationship('Legislation', secondary=activity_legislation_association, back_populates='activities')
    clinics = db.relationship('Clinic', secondary=activity_clinics_association, back_populates='activities')

    def to_dict(self):
        return {
            "id": self.id,
            "title": self.title,
            "date": self.date,
            "time": self.time,
            "state": self.state,
            "venue": self.venue,
            "address": self.address,
            "zip": self.zip,
            "url": self.url
        }

    def __repr__(self):
        return f'ID: {self.id}, Title: {self.title}'

class Legislation(db.Model):
    __tablename__ = 'legislation'
    __searchable__ = ['state', 'date', 'banned_after_weeks_since_LMP', 'exception_fetal', 'exception_health', 'exception_rape_or_incest', 'no_restrictions']

    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.String(20))
    date = db.Column(db.String(25))
    banned_after_weeks_since_LMP = db.Column(db.String(3))
    exception_fetal = db.Column(db.String(50))
    exception_health = db.Column(db.String(50))
    exception_life = db.Column(db.Boolean())
    exception_rape_or_incest = db.Column(db.Boolean())
    no_restrictions = db.Column(db.String(50))

    activities = db.relationship('Activity', secondary=activity_legislation_association, back_populates='legislations')
    clinics = db.relationship('Clinic', secondary=legislation_clinics_association, back_populates='legislations')

    def to_dict(self):
        return {
            "id": self.id,
            "state": self.state,
            "date": self.date,
            "banned_after_weeks_since_LMP": self.banned_after_weeks_since_LMP,
            "exception_fetal": self.exception_fetal,
            "exception_health": self.exception_health,
            "exception_life": self.exception_life,
            "exception_rape_or_incest": self.exception_rape_or_incest,
            "no_restrictions": self.no_restrictions,
        }

    def __repr__(self):
        return f'ID: {self.id}, Title: {self.state}'

class Clinic(db.Model):
    __tablename__ = 'clinics'
    __searchable__ = ['city', 'state_abbr', 'state']

    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(50))
    state_abbr = db.Column(db.String(5))
    state = db.Column(db.String(20))
    travel_time = db.Column(db.Integer)
    travel_distance = db.Column(db.Integer)
    total_females = db.Column(db.Integer)
    num_facilities = db.Column(db.Integer)
    url = db.Column(db.String(500))

    activities = db.relationship('Activity', secondary=activity_clinics_association, back_populates='clinics')
    legislations = db.relationship('Legislation', secondary=legislation_clinics_association, back_populates='clinics')
    
    def to_dict(self):
        return {
            "id": self.id,
            "city": self.city,
            "state_abbr": self.state_abbr,
            "state": self.state,
            "travel_time": self.travel_time,
            "travel_distance": self.travel_distance,
            "total_females": self.total_females,
            "num_facilities": self.num_facilities,
            "url": self.url
        }
    
    def __repr__(self):
        return f'ID: {self.id}, Location: {self.city}, {self.state_abbr}'
    

def getActivities():
    return Activity.query.all()

def getActivitiesByPagination(page, entries, activities=None):
    if activities == None:
        activities = Activity.query.all()
    filteredActivities = []
    if (page-1)*entries <= len(activities):
        filteredActivities = activities[(page-1)*entries:page*entries] if page*entries <= len(activities) else activities[(page-1)*entries:]

    return filteredActivities

def getActivitiesByID(id):
    activity = Activity.query.filter_by(id=id).first().to_dict()

    legislationIDs = [db.session.query(Legislation).join(activity_legislation_association).filter(activity_legislation_association.c.activity_id == activity["id"]).first().id]
    clinicIDs = [clinic.id for clinic in db.session.query(Clinic).join(activity_clinics_association).filter(activity_clinics_association.c.activity_id == activity["id"])]

    activity["legislations"] = legislationIDs
    activity["clinics"] = clinicIDs
    return activity

def getActivitiesSorted(attribute, ascending):
    return Activity.query.order_by(text(f'{attribute} {"asc" if ascending == "True" else "desc"}')).all()

def getActivitiesFilterable(attribute, param):
    filter_params = {attribute: param}
    return Activity.query.filter_by(**filter_params).all() 

def getActivitiesFilterableByPagination(attribute, param, page, entries):
    filter_params = {attribute: param}
    return Activity.query.filter_by(**filter_params).all() 

def getLegislation():
    return Legislation.query.all()

def getLegislationByPagination(page, entries, legislation=None):
    if legislation == None:
        legislation = Legislation.query.all()

    filtered_states = []
    if (page-1)*entries <= len(legislation):
        filtered_states = legislation[(page-1)*entries:page*entries] if page*entries <= len(legislation) else legislation[(page-1)*entries:]
    
    return filtered_states

def getLegislationByID(id):
    legislation = Legislation.query.filter_by(id=id).first().to_dict()
    activityIDs = [activity.id for activity in db.session.query(Activity).join(activity_legislation_association).filter(activity_legislation_association.c.legislation_id == legislation["id"])]
    clinicIDs = [clinic.id for clinic in db.session.query(Clinic).join(legislation_clinics_association).filter(legislation_clinics_association.c.legislation_id == legislation["id"])]

    legislation["activities"] = activityIDs
    legislation["clinics"] = clinicIDs

    return legislation

def getLegislationSorted(attribute, ascending):
    print(Legislation.query.order_by(text(f'{attribute} {"asc" if ascending == "True" else "desc"}')))
    return Legislation.query.order_by(text(f'{attribute} {"asc" if ascending == "True" else "desc"}')).all()

def getLegislationFilterable(attribute, param):
    if param == "true":
        param = 1
    elif param == "false":
        param = 0
    filter_params = {attribute: param}
    return Legislation.query.filter_by(**filter_params).all()

def getClinics():
    return Clinic.query.all()
    
def getClinicsByPagination(page, entries, clinics=None):
    if clinics == None:
        clinics = Clinic.query.all()

    filtered_clinics = []

    if (page-1)*entries <= len(clinics):
        filtered_clinics = clinics[(page-1)*entries:page*entries] if page*entries <= len(clinics) else clinics[(page-1)*entries:]
    
    return filtered_clinics
    
def getClinicsByID(id):
    clinic = Clinic.query.filter_by(id=id).first().to_dict()
    activityIDs = [activity.id for activity in db.session.query(Activity).join(activity_clinics_association).filter(activity_clinics_association.c.clinics_id == clinic["id"])]
    legislationIDs = [legislation.id for legislation in db.session.query(Legislation).join(legislation_clinics_association).filter(legislation_clinics_association.c.clinics_id == clinic["id"])]

    clinic["activities"] = activityIDs
    clinic["legislations"] = legislationIDs
    return clinic

def getClinicsSorted(attribute, ascending):
    return Clinic.query.order_by(text(f'{attribute} {"asc" if ascending == "True" else "desc"}')).all()

def getClinicsFilterable(attribute, param):
    filter_params = {attribute: param}
    return Clinic.query.filter_by(**filter_params).all()

# HELPER FUNCTIONS
def getImageURL(search):
    PEXEL_API_KEY = os.environ.get("PEXEL_API_KEY")
    url = "https://api.pexels.com/v1/search"

    headers = {"Authorization": PEXEL_API_KEY}
    params = {
        "query": search,
        "per_page": 1
    }
    photos = requests.get(url, headers=headers, params=params).json().get('photos')
    if not photos:
        return "https://images.pexels.com/photos/325185/pexels-photo-325185.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
    else:
        return requests.get(url, headers=headers, params=params).json()['photos'][0]['src']['original']

# SEARCH FUNCTIONS
def searchActivities(query):
    res = []
    searchable = ['title', 'date', 'time', 'state', 'venue', 'address', 'zip']
    for col in searchable:
        result = Activity.query.filter(col.contains(query))
        if result.first():
            res.append(result.to_dict())
    
    query_terms = []
    for term in query:
        for col in searchable:
            query_terms.append(eval(f"Activity.{col}.contains(term)"))
        result = Activity.query.filter



def searchLegislation(query):
    pass

def searchClinics(query):
    pass

# POPULATION SCRIPTS
def populateActivities():
    activities = []

    options = webdriver.ChromeOptions()
    options.add_argument("--headless=new")
    driver = webdriver.Chrome(options=options)
    driver.get("https://act.plannedparenthoodaction.org/events?page=1")

    def scrapePage():
        driver.refresh()
        elements = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME, "main-info"))
        )
        for element in elements:
            title = element.find_element(By.XPATH, './/*[@class="title text-primary"]').text
            date = element.find_element(By.CLASS_NAME, 'detail-text').text
            
            time = element.find_elements(By.XPATH, './/*[@class="detail-with-icon time"]')
            time = None if not time else time[0].find_element(By.CLASS_NAME, 'detail-text').text

            venue = element.find_elements(By.XPATH, './/*[@class="detail-with-icon"]')
            venue = None if not venue else venue[0].find_elements(By.CLASS_NAME, 'detail-text')
            venue = None if not venue else venue[0].find_elements(By.CLASS_NAME, 'venue')
            venue = None if not venue else venue[0].text

            address = element.find_elements(By.XPATH, './/*[@class="detail-with-icon"]')
            address = None if not address else address[0].find_elements(By.CLASS_NAME, 'detail-text')
            address = None if not address else address[0].find_elements(By.CLASS_NAME, 'full-address')
            address = None if not address else address[0].text

            if '—' in date:
                date = date.split('—')[0].strip()
            date = str(parser.parse(date).date())
            
            if time:
                start, end = time.split('—')
                start, end = start.strip(), end.split()
                end = end[0] + end[1]
                start, end = parser.parse(start).time(), parser.parse(end).time()
                time = f'{start} - {end}'
            if address:
                zip_code, state = address.split(',')[-1].strip(), address.split(',')[1].strip()
                if state not in STATE_DICT or len(zip_code) < 5:
                    continue

                activity = Activity(title=(title if title else "N/A"), 
                                    date=(date if date else "N/A"), 
                                    time=(time if time else "N/A"), 
                                    state=STATE_DICT[state],
                                    venue=(venue if venue else "N/A"), 
                                    address=(address if address else "N/A"),
                                    zip=(zip_code if zip_code else "N/A"),
                                    url=getImageURL(address.split(",")[0] + " " + STATE_DICT[state])
                                )
                db.session.add(activity)
        
    try:
        # page 1
        while True:
            scrapePage()
            driver.find_element(By.XPATH, '//*[@class="page-link next-button"]').click()
    except NoSuchElementException:
        print("INFO: Activity scraping finished")
        db.session.commit()
        print("INFO: Activity table populated")
    finally:
        driver.quit()

def populateLegislation():
    apikey = os.environ.get('ABORTION_API_KEY')
    url = 'https://api.abortionpolicyapi.com/v1/gestational_limits/states/'

    headers = {'token': apikey}

    states = requests.get(url, headers=headers).json()
    print("INFO: Legislation API pulled")
    for i in range(len(STATE_LIST)):
        state = STATE_LIST[i]
        legislation = Legislation(id=i+1, state=STATE_LIST[i],
                    date=states[state].get('Last Updated'), 
                    banned_after_weeks_since_LMP=states[state].get('banned_after_weeks_since_LMP', 'N/A'),
                    exception_fetal=states[state].get('exception_fetal', 'N/A'),
                    exception_health=states[state].get('exception_health', 'N/A'),
                    exception_life=states[state].get('exception_life', False),
                    exception_rape_or_incest=states[state].get('exception_rape_or_incest', False),
                    no_restrictions=states[state].get('no_restrictions', False)
                )
        db.session.add(legislation)
    db.session.commit()
    print("INFO: Legislation table populated")
    
def populateClinics():
    url = 'https://services1.arcgis.com/hLJbHVT9ZrDIzK0I/arcgis/rest/services/ScenarioMAB_Surplus_in_Average_Service_Population/FeatureServer/106/query?outFields=*&where=1%3D1&f=geojson'
    
    clinics = requests.get(url).json().get('features')
    
    print("INFO: Clinics API pulled")
    for clinic in clinics:
        props = clinic['properties']
        clinic = Clinic(id=clinic['id'],city=props['Location_Name'].split(',')[0],
            state_abbr=props['STATE_ABBREV'],
            state=props['STATE_NAME'],
            travel_time=int(props['mean_mean_total_traveltime']),
            travel_distance=int(props['mean_mean_total_miles']),
            total_females=props['sum_sum_females_age_15_44'],
            num_facilities=props['Point_Count'],
            url=getImageURL(props['Location_Name'].split(',')[0])
        )
        db.session.add(clinic)
    db.session.commit()
    print("INFO: Clinics table populated")

def populateActivityLegislation():
    for activity in Activity.query.all():
        filteredLegislation = Legislation.query.filter_by(state=activity.state)
        for legislation in filteredLegislation:
            activity.legislations.append(legislation)
            # ala = activity_legislation_association(activity.id, state.id)
            # db.session.add(ala)
    db.session.commit()

def populateActivityClinics():
    for activity in Activity.query.all():
        filteredClinics = Clinic.query.filter_by(state=activity.state)
        for clinic in filteredClinics:
            activity.clinics.append(clinic)
    db.session.commit()

def populateLegislationClinics():
    for clinic in Clinic.query.all():
        legislations = Legislation.query.filter_by(state=clinic.state)
        for legislation in legislations:
            clinic.legislations.append(legislation)
    db.session.commit()

def populate():
    db.create_all()
    populateActivities()
    # populateLegislation()
    # populateClinics()

def populateAssociations():
    populateActivityLegislation()
    populateActivityClinics()
    populateLegislationClinics()

def reset():
    db.drop_all()

if __name__ == '__main__':
    with application.app_context():
        reset()
        populate()
        # populateAssociations()
