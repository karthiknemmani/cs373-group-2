# Write 10 testcases for navigation of the frontend using selenium.

# Path: frontend/ui_tests.py
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException
from selenium.webdriver.common.action_chains import ActionChains
import unittest

from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import ElementClickInterceptedException
import time
class TestAppNavigation(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # Configure Selenium WebDriver (use your preferred browser driver)
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(10)  # Implicit wait for element presence
        self.driver.get("https://www.choiceconnect.me/")
        self.wait = WebDriverWait(self.driver, 10)
        self.driver.maximize_window()

    # def tearDown(self):
    #     self.driver.close()

    def navigate_to_page(self, link_text):
        link = self.driver.find_element(By.LINK_TEXT, link_text)
        link.click()

    def test_about_page_navigation(self):
        # Test navigation from Home to About
        self.navigate_to_page("About")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/about/", "Navigating to About did not redirect to the expected URL.")
    
    def test_home_page_navigation(self):
        # Test navigation from About back to Home
        self.navigate_to_page("ChoiceConnect")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/", "Navigating to Home did not redirect to the expected URL.")

    def test_legislation_navigation(self):
        # Test navigation from Home to Legislation
        self.navigate_to_page("Legislation Information")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/legislation/", "Navigating to Legislation did not redirect to the expected URL.")
    
    # def test_legislation_instance_navigation(self):  
    #     self.navigate_to_page("Legislation Information")
    #     # Wait for the "Alabama" element to be clickable, indicating it's fully loaded and interactable
    #     next = self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'Alabama')]")))
    #     next.click()
    #     # Assert that the current URL is as expected after navigation
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/"), "Navigating to Legislation instance did not redirect to the expected URL.")

        
    def test_community_page_navigation(self):
        # Test navigation from Home to Community Activities
        self.navigate_to_page("Community Activities")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/community/", "Navigating to Community Activities page did not redirect to the expected URL.")
    
    def test_critiques_page_navigation(self):
        # Test navigation from Home to Visualization
        self.navigate_to_page("Critiques")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/critiques/", "Navigating to Critiques page did not redirect to the expected URL.")
        
    def test_clinics_page_navigation(self):
        # Test navigation from Home to Practices & Clinics
        self.navigate_to_page("Cities")
        self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/practices/", "Navigating to Practices & Clinics page did not redirect to the expected URL.")

    # def test_cities_pagination(self):
    #     self.navigate_to_page("Cities")

    #     # Attempt to find the "Next" button. Ensure it's visible and clickable.
    #     try:
    #         next_button = WebDriverWait(self.driver, 10).until(
    #             EC.visibility_of_element_located((By.LINK_TEXT, "Next"))
    #         )

    #         # Scroll the "Next" button into view
    #         self.driver.execute_script("arguments[0].scrollIntoView(true);", next_button)

    #         # Wait a moment for animations or overlays to clear
    #         WebDriverWait(self.driver, 2).until(EC.element_to_be_clickable((By.LINK_TEXT, "Next")))

    #         # Use ActionChains as an alternative method to click, providing more flexibility
    #         ActionChains(self.driver).move_to_element(next_button).click().perform()

    #     except TimeoutException:
    #         self.fail("Timed out waiting for the 'Next' button to become visible and clickable.")
    #     except ElementClickInterceptedException:
    #         self.fail("'Next' button click was intercepted by another element.")

    #     # Verify the URL to confirm that the pagination worked as expected
    #     WebDriverWait(self.driver, 10).until(lambda d: self.driver.current_url.startswith("https://www.choiceconnect.me/practices/"))
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/"), "Navigating to page 2 did not redirect to the expected URL.")

    def test_activities_pagination(self):
        self.navigate_to_page("Community Activities")

        # Attempt to find the "Next" button. Ensure it's visible and clickable.
        try:
            next_button = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.LINK_TEXT, "Next"))
            )

            # Scroll the "Next" button into view
            self.driver.execute_script("arguments[0].scrollIntoView(true);", next_button)

            # Wait a moment for animations or overlays to clear
            WebDriverWait(self.driver, 2).until(EC.element_to_be_clickable((By.LINK_TEXT, "Next")))

            # Use ActionChains as an alternative method to click, providing more flexibility
            ActionChains(self.driver).move_to_element(next_button).click().perform()

        except TimeoutException:
            self.fail("Timed out waiting for the 'Next' button to become visible and clickable.")
        except ElementClickInterceptedException:
            self.fail("'Next' button click was intercepted by another element.")

        # Verify the URL to confirm that the pagination worked as expected
        WebDriverWait(self.driver, 10).until(lambda d: self.driver.current_url.startswith("https://www.choiceconnect.me/community/"))
        self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/"), "Navigating to page 2 did not redirect to the expected URL.")
    
    def test_search_is_in_nav(self):
        search = self.driver.find_element(By.XPATH, "//input[@type='search']")
        self.assertTrue(search.is_displayed(), "Search bar is not displayed in the navigation bar.")
    
    def test_search_in_legislation(self):
        self.navigate_to_page("Legislation Information")
        search = self.driver.find_element(By.XPATH, "//input[@type='search']")
        self.assertTrue(search.is_displayed(), "Search not in legislation.")
        
    def test_search_in_activities(self):
        self.navigate_to_page("Community Activities")
        search = self.driver.find_element(By.XPATH, "//input[@type='search']")
        self.assertTrue(search.is_displayed(), "Search not in community.")
    # def test_community_instance_navigation(self):
    #     self.navigate_to_page("Community Activities")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'PPAA WKY: Paducah Book Club')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'PPAA WKY: Paducah Book Club')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/3"), "Navigating to Community Activities instance did not redirect to the expected URL.")
    
    # def test_practices_navigation(self):
    #     # Test navigation from Home to Community Activities
    #     self.navigate_to_page("Practices & Clinics")
    #     self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/practices/", "Navigating to Community Activities page did not redirect to the expected URL.")
    
    # def test_practices_instance_navigation(self):
    #     self.navigate_to_page("Practices & Clinics")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Ann Arbor')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Ann Arbor')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/5"), "Navigating to Practices instance did not redirect to the expected URL.")
    
    # def test_legislation_instance_connection_events(self):
    #     self.navigate_to_page("Legislation Information")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Alaska')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Alaska')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/2"), "Navigating to Legislation instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Events')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/"), "Navigating to Nearby Events did not redirect to the expected URL.")
    
    # def test_legislation_instance_connection_clinics(self):
    #     self.navigate_to_page("Legislation Information")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Arizona')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Arizona')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/3"), "Navigating to Legislation instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Clinics')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/95"), "Navigating to Nearby Events did not redirect to the expected URL.")
    
    # def test_events_instance_connection_legislation(self):
    #     self.navigate_to_page("Community Activities")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Office Volunteer Hours')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Office Volunteer Hours')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/1"), "Navigating to Community instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'State Legislation')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/"), "Navigating to Legislation instance did not redirect to the expected URL.")
        
    # def test_events_instance_connection_clinics(self):
    #     self.navigate_to_page("Community Activities")
    #     self.driver.maximize_window()
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Office Volunteer Hours')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Office Volunteer Hours')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/1"), "Navigating to Community instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Clinics')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/"), "Navigating to Practice instance did not redirect to the expected URL.")
        
    # def test_events_instance_connection_clinics(self):
    #     self.navigate_to_page("Practices & Clinics")
    #     self.driver.maximize_window()
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Ann Arbor')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Ann Arbor')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/5"), "Navigating to Practice instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Events')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/"), "Navigating to Nearby Events did not redirect to the expected URL.")
    
    # def test_clinics_instance_connection_events(self):
    #     self.navigate_to_page("Legislation Information")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Alaska')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Alaska')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/2"), "Navigating to Legislation instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Events')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/community/"), "Navigating to Nearby Events did not redirect to the expected URL.")
    
    # def test_clinics_instance_connection_legislation(self):
    #     self.navigate_to_page("Legislation Information")
    #     self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Arizona')]")))
    #     self.driver.find_element(By.XPATH, "//div[contains(text(), 'Arizona')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/legislation/3"), "Navigating to Legislation instance did not redirect to the expected URL.")
    #     self.driver.find_element(By.XPATH, "//a[contains(text(), 'Nearby Clinics')]").click()
    #     self.assertTrue(self.driver.current_url.startswith("https://www.choiceconnect.me/practices/"), "Navigating to Nearby Events did not redirect to the expected URL.")
        
        
    # def test_collapsed_menu(self):
    #     # Shrink window to test collapsed menu
    #     self.driver.set_window_size(800, 600)
    #     self.driver.find_element(By.CLASS_NAME, "navbar-toggler").click()
    #     self.assertTrue(self.driver.find_element(By.CLASS_NAME, "navbar-collapse").is_displayed(), "Collapsing the menu did not display the menu.")
    #     self.driver.maximize_window()
    
#         # Test navigation from Housing instance back to Housing
#         self.driver.back()
#         self.wait.until(not (EC.url_contains("/1")))
#         self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/housing/", "Going back from Housing instance did not redirect to Housing.")

#     def test_community_page_navigation(self):
#         # Test navigation from Home
#         self.navigate_to_page("Community Activities")
#         self.assertEqual(self.driver.current_url, "https://www.choiceconnect.me/community/", "Navigating to Community Activities page did not redirect to the expected URL.")

if __name__ == "__main__":
    unittest.main()