import React, { useState, useEffect } from "react";

import tarun_img from "../assets/tarun.jpg"
import ethan_img from "../assets/ethan.jpg"
import karthik_img from "../assets/karthik.jpg"
import shreyansh_img from "../assets/shreyansh.jpg"
import shrey_img from "../assets/shrey.jpg"
import docker_img from "../assets/docker_img.webp"
import python_img from "../assets/python_img.png"
import postman_img from "../assets/postman_img.svg"
import vscode_img from "../assets/vscode_img.webp"
import react_img from "../assets/react_img.png"
import gitlab_img from "../assets/gitlab_img.svg"


export const team = [
    {
      img_src: tarun_img,
      name: "Tarun Mohan",
      email: "tarun.mohan@utexas.edu",
      gitlab_username: "tarun.mohan",
      description:
        "I'm a second-year CS major at UT Austin interested in generative AI. I enjoy playing basketball and trying new restaurants.",
      role: "Full-Stack",
      commits: 0,
      issues: 0,
      link: "https://www.linkedin.com/in/tarun-mohan/"
    },
    {
      img_src: ethan_img,
      name: "Ethan Benson",
      email: "ethanrbenson@gmail.com",
      gitlab_username: "qquiche",
      description:
        "I am a second-year CS major at UT Austin interested in software engineering and data science. I enjoy playing guitar and reading books.",
      role: "Backend",
      commits: 0,
      issues: 0,
      link: "https://www.linkedin.com/in/ethan-benson-064ba9151/"
    },
    {
      img_src: karthik_img,
      name: "Karthik Nemmani",
      email: "karthiknemmani@utexas.edu",
      gitlab_username: "karthiknemmani",
      description:
        "I'm a second-year CS major at UT Austin interested in computational linguistics. I enjoy watching sports and traveling.",
      role: "Frontend",
      commits: 0,
      issues: 0,
      link: "https://www.linkedin.com/in/karthik-nemmani/"
    },
    {
      img_src: shreyansh_img,
      name: "Shreyansh Dixit",
      email: "shreyansh.dixit@gmail.com",
      gitlab_username: "shreyanshd43",
      description:
        "I'm a second-year CS major interested in software engineering and AI. I enjoy making films and composing music.",
      role: "Frontend",
      commits: 0,
      issues: 0,
      link: "https://www.linkedin.com/in/shreyansh-dixit/"
    },
    {
      img_src: shrey_img,
      name: "Shrey Gupta",
      email: "shreygupta04@gmail.com",
      gitlab_username: "shreygupta04",
      description:
        "Hey, I'm a CS major planning on minoring in economics. I'm passionate about running, movies, and listening to bands.",
      role: "Backend",
      commits: 0,
      issues: 0,
      link: "https://www.linkedin.com/in/shreygu/"
    },
  ];
  
  export const tools = [
    { name: "Python", logo: python_img, link: "https://www.python.org/"},
    { name: "React.js", logo: react_img, link: "https://reactjs.org/"},
    { name: "Docker", logo: docker_img, link: "https://www.docker.com/"},
    { name: "Postman", logo: postman_img, link: "https://www.postman.com/"},
    { name: "VS Code", logo: vscode_img, link: "https://code.visualstudio.com/"},
    { name: "Gitlab", logo: gitlab_img, link: "https://about.gitlab.com/"}
  ];
  
  export const data_sources = [
    {
      name: "Abortion Policy API",
      link: "https://www.abortionpolicyapi.com/",
      description: "Getting Abortion Policy Information",
    },
    {
      name: "Abortion Access Dashboard",
      link: "https://about-the-abortion-access-dashboard-analysis-1.hub.arcgis.com/datasets/a18d275d534c4d22b1a9dac16c25d920_106/explore",
      description: "Clinic Information in US Metropolitan Areas",
    },
    {
      name: "Planned Parenthood",
      link: "https://act.plannedparenthoodaction.org/events",
      description: "Community Events Information",
    },
  ];
  