import lobbyday_img from "../assets/LobbyDay.png";
import PPAA_img from "../assets/PPAA.png";
import valDay_img from "../assets/ValentinesDay.png";
const eventData = {"events":[  
    {
        "id": 1,
        "title": "Lobby Day 2024",
        "inPerson": true,
        "date": "February 14, 2024",
        "location": "General Assembly Building (GAB)",
        "city": "Richmond",
        "state": "Virginia",
        "zipcode": 23219,
        "time": "8:00 AM - 1:00 PM ET",
        "eventType": "Coalition Event",
        "description": "Join us on Valentine's Day to PINK OUT the state capitol and lobby for reproductive rights! We'll be coming together to celebrate our victories in 2023 and push our legislators to continue to work to make healthcare accessible to all.",
        "image": lobbyday_img,
        "image_link": "https://www.google.com/maps?rlz=1C1CHZN_enUS968US968&gs_lcrp=EgZjaHJvbWUqBggAEEUYOzIGCAAQRRg7MgYIARBFGDkyBwgCEAAYjwIyBggDEEUYQDIGCAQQRRg8MgYIBRBFGD0yBggGEEUYPNIBCDI2NDFqMGo0qAIAsAIA&um=1&ie=UTF-8&fb=1&gl=us&sa=X&geocode=KUXKS-IiEbGJMZe_IDAIy9yT&daddr=201+N+9th+St,+Richmond,+VA+23219"
    },
    {
        "id": 2,
        "title": "PPAA WA: Pack the House for KOCA",
        "inPerson": true,
        "date": "February 14, 2024",
        "location": "Washington State Capitol",
        "city": "Olympia",
        "state": "Washington",
        "zipcode": 98501,
        "time": "8:00 AM - 12:00 PM PT",
        "eventType": "Coalition Event",
        "description": "Join activists to pack the hearing room where the house will be listening to testimony on the Keep Our Care Act. Come in your best pink shirt outfit and show your support!",
        "image": PPAA_img,
        "image_link": "https://www.google.com/maps/place/Washington+State+Capitol+Building,+416+Sid+Snyder+Ave+SW,+Olympia,+WA+98504/data=!4m2!3m1!1s0x549174fdce832075:0x950859602d9e1c?sa=X&ved=2ahUKEwidwfXXmqyEAxUtkiYFHSDuCqwQ8gF6BAheEAA"
    },
    {
        "id": 3,
        "title": "Valentine's Day Safe Sex Kit Distribution",
        "inPerson": true,
        "date": "February 14, 2024",
        "location": "FAMU Quad",
        "city": "Tallahassee",
        "state": "Florida",
        "zipcode": 32307,
        "time": "11:00 AM - 1:00 PM ET",
        "eventType": "Health Center",
        "description": "Passing out Safe Sex Kits on Valentine's Day on the Quad",
        "image": valDay_img,
        "image_link": "https://www.google.com/maps/dir//Florida+A%26M+University,+1601+S+Martin+Luther+King+Jr+Blvd,+Tallahassee,+FL+32307/@30.4214459,-84.2973595,15z/data=!4m18!1m8!3m7!1s0x88ec5f8c00000001:0x6c6eb455af435a90!2sFlorida+A%26M+University!8m2!3d30.4227211!4d-84.2877093!15sCglGQU1VIFF1YWSSAQp1bml2ZXJzaXR54AEA!16zL20vMDJxdnZ2!4m8!1m0!1m5!1m1!1s0x88ec5f8c00000001:0x6c6eb455af435a90!2m2!1d-84.2877093!2d30.4227211!3e2?entry=ttu"
    }
]}; 
export default eventData;