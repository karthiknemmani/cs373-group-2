import React, { useEffect, useState, useCallback } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PureComponent } from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer, BarChart, Bar, Rectangle, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Cell, Sector } from 'recharts';
import axios from 'axios';


const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
    state
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`${state} ${value}`}</text>
    </g>
  );
};

function Critiques() {
  const scrollableMenuStyle = {
    maxHeight: '25rem',
    overflowY: 'scroll',
  }

  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );

  return (
    <Container fluid>
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style">Critiques</h1>
        <p className="Paragraph-style-dg-center" style={{ marginTop: '-6%' }}>
            The team showcased effective division of labor and communication between teams, successfully establishing the
            middleware between Flask and React while developing Flask solely as a backend API. We also made good use of peer reviews, 
            ensuring that our site is accessible and interactive from an outside perspective, using their feedback to make any changes
            we might not have considered before. Areas for improvement include better design of the backend class structure, earlier
            initiation of AWS hosting, and conducting peer reviews to gain outside perspectives on potential UI changes. However, some
            lingering confusions remain, such as how the website will scale with auto-balancing to handle increased traffic, what
            mechanisms are in place to protect sensitive database data, and what existing search algorithms can be implemented for faster
            searches as more data is added.
          </p>
      </Row>
      <Row className="Page-dp">
        <h1 className="Header-style-alternate" style={{ marginBottom: '4%' }}>Provider Critiques</h1>
        <p className="Paragraph-style-lg-center">
            From their website, we can gain insights into the types of job opportunities available to ex-criminals, which can
            greatly aid in facilitating their post-prison employment pipeline. One area for improvement is the extended initial
            load times for their model pages, likely due to querying all pages simultaneously; implementing a pagination API call
            could expedite this process. Additionally, while the county cards present a wealth of information, the categorization
            and organization of data may not be intuitive or readily understandable to users, causing confusion. By addressing these areas,
            the platform can enhance its user experience while concurrently shedding light on employment prospects for formerly incarcerated 
            individuals, ultimately benefiting both the platform's accessibility and the reintegration efforts for this demographic.
          </p>
      </Row>
    </Container>
  );
}

export default Critiques;
