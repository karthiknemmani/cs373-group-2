import React, { useEffect, useState } from 'react';
import { Col, Container, Row, Table } from 'react-bootstrap';
import MemberCard from '../components/MemberCard';
import { team as initialTeam } from '../data/about';
import { data_sources } from '../data/about';
import { tools } from '../data/about';
import ToolCard from '../components/ToolCard';
import axios from 'axios';

function About() {
  const [team, setTeam] = useState(initialTeam);
  const [numCommits, setNumCommits] = useState(0);
  const [numIssues, setNumIssues] = useState(0);

  useEffect(() => {
    const gitlabToken = 'glpat-aUKyzxAGyr88cPpY9jfy'; // Store your token in an .env file
    const projectId = '54659571'; // Replace with your actual project ID

    const headers = {
      'PRIVATE-TOKEN': gitlabToken,
    };

    const fetchGitLabData = async () => {
      try {
        const response = await axios.get(`https://api.choiceconnect.me/about`);
        console.log(response);
        setNumCommits(response.data.totals.commits);
        setNumIssues(response.data.totals.issues);

        // Example of processing commits and issues
        const updatedTeam = team.map(member => {
          const memberCommits = response.data.members[member.name].commits;
          const memberIssues = response.data.members[member.name].issues;
          return {
            ...member,
            commits: memberCommits,
            issues: memberIssues
          }
        });

        setTeam(updatedTeam);
      } catch (error) {
        console.error('Error fetching data from GitLab:', error);
      }
    };

    fetchGitLabData();
  }, []);

  return (
    <div>
      <Container fluid className="m-0 p-0">
        <Row className="Page-lp">
          <div>
            <h1 className="Header-style">Meet the Team</h1>
          </div>

          <div className="d-flex justify-content-center flex-wrap mb-5">
            {Object.keys(team).map((memberName, index) => (
                <a href = {team[memberName].link} className="text-decoration-none" key={index}>
                  <MemberCard
                    key={index}
                    name={team[memberName].name}
                    img={team[memberName].img_src}
                    bio={team[memberName].description}
                    role={team[memberName].role}
                    commits={team[memberName].commits}
                    issues={team[memberName].issues}
                    className="Card"
                  />
                </a>
            ))}
          </div>

          {/* <div className="row mx-auto mb-5 justify-content-center">
            {Object.keys(team).map((memberName, index) => (
                <MemberCard
                  key={index}
                  name={team[memberName].name}
                  img={team[memberName].img_src}
                  bio={team[memberName].description}
                  role={team[memberName].role}
                  commits={team[memberName].commits}
                  issues={team[memberName].issues}
                  className="Card"
                />
            ))}
          </div> */}

          <h1 className="Title-style-dp">Site Description</h1>
          <p className="Paragraph-style-dg-center">
            This website is intended to be a source of information about current legal 
            policy on abortion in the United States and the resources available. Its purpose
            is to create an accessible platform dedicated to providing crucial resources to ultimately
            ensure safe reproduction and medical care among American women. The intended users
            are those seeking more information about abortion and reproductive rights.
          </p>
          <p className="Paragraph-style-dg-center" style={{marginBottom: '3rem'}}>
            Dealing with disparate data can be complex because it requires identifying connections across various sources.
            These databases contain diverse sets of information, necessitating careful selection of the relevant data points.
          </p>

          <h1 className="Title-style-dp">GitLab Repo Totals</h1>
          <p className="Paragraph-style-dg-center">
            Total commits: {numCommits}
          </p>
          <p className="Paragraph-style-dg-center" style={{marginBottom: '3rem'}}>
            Total issues: {numIssues}
          </p>
          {/* <h4 className="text-center mb-5">Total issues: {issueData.length}</h4> */}
          
          <h1 className="Title-style-dp">Data Sources</h1>
          <Row className="m-0 justify-content-center mb-5">
            <div style={{ paddingRight: "15%", paddingLeft: "15%" }}>
              <Table striped bordered hover size="sm" className="Table-style-dp" style={{marginBottom: '3rem'}}>
                <thead>
                  <tr align='center'>
                    <th className="Card-title-lg">Source</th>
                    <th className="Card-title-lg">Description</th>
                  </tr>
                </thead>
                <tbody>
                  {data_sources.map((source) => (
                    <tr>
                      <td align='center'>          
                        <a
                          className='Card-text-dp'
                          style={{ color: 'black', textDecoration: 'underline' }}
                          href={source.link}>
                          {source.name}
                        </a>
                      </td>
                      <td className='Card-text-dp' align='center'>
                        {source.description}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </Row>
          
          <h1 className="Title-style-dp">Tools Used</h1>
          <div className="d-flex justify-content-center flex-wrap mb-5">
            {tools.map((tool, index) => (
              <a key={index} href={tool.link} className="text-decoration-none m-0.5">
                <ToolCard name={tool.name} img={tool.logo}></ToolCard>
              </a>
            ))}
          </div>

            <Row className="m-0 d-flex justify-content-center mb-5" style={{ marginBottom: '4rem' }}>
              <Col xs="auto">
                <div className="Card-title-lg">
                  <a
                    href="https://documenter.getpostman.com/view/32889098/2sA2r6WPJ9"
                    className="btn btn-primary p-4 Button-dp"
                  >
                    <h4 style={{ marginBottom: "0" }}>API Documentation</h4>
                  </a>
                </div>
              </Col>
              <Col xs="auto">
                <div className="Card-title-lg">
                  <a
                    href="https://gitlab.com/karthiknemmani/cs373-group-2"
                    className="btn btn-primary p-4 Button-dp" /* Add bootstrap button classes here */
                  >
                    <h4 style={{ marginBottom: "0" }}>GitLab Repository</h4>
                  </a>
                </div>
              </Col>
            </Row>
        </Row>
      </Container>
    </div>
  );
}

export default About;
