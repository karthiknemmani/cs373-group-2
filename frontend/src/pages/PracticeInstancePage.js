import { useParams, Link } from "react-router-dom";
import practiceData from '../data/practices.json';
import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBuilding, faStar, faMapMarkerAlt, faClock, faRoad, faFemale, faMapPin } from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import Map from '../components/MapComponent';
import Connections from "../components/Connections";
import EventInstance from "../components/EventInstance";
import PolicyInstance from "../components/PolicyInstance";
import Pagination from "../components/Pagination";

function PracticeInstancePage() {
    const {practiceId} = useParams();
    const [practiceInstance, setPracticeInstance] = useState(null); // Use null for initial state

    const [legislationData, setLegislationData] = useState(null);
    const [eventData, setEventData] = useState(null);

    const currentDate = new Date(); // Get current date
    const year = currentDate.getFullYear(); // Get the year
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Get the month and pad with zero if needed
    const day = String(currentDate.getDate()).padStart(2, '0'); // Get the day and pad with zero if needed
    const formattedDate = `${year}-${month}-${day}`; // Format the date as "YYYY-MM-DD"

    const [currentPageLeg, setCurrentPageLeg] = useState(0);
    const [currentPageEvent, setCurrentPageEvent] = useState(0);
    const [totalPagesLeg, setTotalPagesLeg] = useState(0);
    const [totalPagesEvent, setTotalPagesEvent] = useState(0);
    const [totalItemLeg, setTotalItemsLeg] = useState(0);
    const [totalItemEvent, setTotalItemsEvent] = useState(0);
    const itemsPerPage = 9;

    const handlePageChangeLeg = (selectedPage) => {
        setCurrentPageLeg(selectedPage.selected);
    }

    const handlePageChangeEvent = (selectedPage) => {
        setCurrentPageEvent(selectedPage.selected);
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`https://api.choiceconnect.me/clinics?id=${practiceId}`);
                // console.log(response.data);
                setPracticeInstance(response.data);
            } catch (error) {
                console.log(error);
            }
        }
        fetchData();
    }, [practiceId]); // Add practiceId to the dependency array to re-run the effect when it changes


    useEffect(() => {
        const getModelData = async () => {
            try {
              let apiUrl = `https://api.choiceconnect.me/clinics?id=${practiceId}`;
              // If filtering is enabled and it's an external filter, modify the API URL
              const response = await axios.get(apiUrl);
      
              const legislationIds = response.data.legislations;

            //   console.log(legislationIds);
              const eventIds = response.data.activities;


      
              let legislationData = await Promise.all(legislationIds.map(async (id) => {
                  const legislationResponse = await axios.get(`https://api.choiceconnect.me/legislation?id=${id}`);
                  return legislationResponse.data;
              }));

            //   console.log(legislationData);

              let eventData = await Promise.all(eventIds.map(async (id) => {
                const eventResponse = await axios.get(`https://api.choiceconnect.me/activities?id=${id}`);
                return eventResponse.data;
            }));

            setTotalItemsLeg(legislationData.length);
            setTotalPagesLeg(Math.ceil(legislationData.length / itemsPerPage));
            setTotalItemsEvent(eventData.length);
            setTotalPagesEvent(Math.ceil(eventData.length / itemsPerPage));

            eventData = eventData.slice(currentPageEvent * itemsPerPage, (currentPageEvent + 1) * itemsPerPage);
            legislationData = legislationData.slice(currentPageLeg * itemsPerPage, (currentPageLeg + 1) * itemsPerPage);

            setLegislationData(legislationData);
            setEventData(eventData);
            } catch (error) {
              console.log(error);
            }
          };
          getModelData();
    }, [practiceId, currentPageLeg, currentPageEvent]);

    // console.log(practiceInstance);
    if (!practiceInstance) {
        return <div>Loading...</div>; // Display loading or not found message
    }
    

    return (
        <div>
            <Container fluid>
                <Row className="Page-lp" style={{ paddingLeft: '15%' , paddingRight: '15%'}}>
                    <Row className="justify-content-start">
                        {/* Title */}
                        <Col xs={12} className="mt-5">
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>{practiceInstance.city}, {practiceInstance.state_abbr}</h1>
                        </Col>

                        {/* Content */}
                        <Row className="mt-4 Card-text-dg" style={{ textAlign: 'left' }}>
                            {/* Left Box with Text */}
                            <Col xs={12} md={6}>
                                <div className="p-4 bg-light" style={{width: '100%'}}>
                                    <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                        <p>
                                            <FontAwesomeIcon icon={faFemale} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                        </p>
                                        <p>
                                            Women of Reproductive Age: {practiceInstance.total_females.toLocaleString()}
                                        </p>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faClock} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                Average Time to Clinic: {practiceInstance.travel_time.toLocaleString()} mins
                                            </p>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faRoad} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                Average Distance to Clinic: {practiceInstance.travel_distance.toLocaleString()} miles
                                            </p>
                                        </Col>
                                    </Row>
                                    <Row style={{marginBottom: '1.2rem'}}>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faBuilding} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                Number of Facilities: {practiceInstance.num_facilities}
                                            </p>
                                        </Col>
                                    </Row>


                                    {/* <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faClock} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <Table striped bordered hover size="sm" responsive="md" className="Table-style-dp" style={{marginBottom: '3rem'}} xs={12} md={4}>
                                                <thead>
                                                    <tr>
                                                        <th className="Card-title-lg" style={{minWidth: '1rem'}}>Day</th>
                                                        <th className="Card-title-lg" style={{minWidth: '1rem'}}>Timings</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {practiceInstance.hours && Object.entries(practiceInstance.hours).map(([day, time], index) => (
                                                    <tr key={index} >
                                                        <td>{day}</td>
                                                        <td>{time}</td>
                                                    </tr>
                                                    ))}
                                                </tbody>
                                            </Table>
                                        </Col>
                                    </Row> */}
                                    {/* Date with Icon */}
                                    <Row style={{marginBottom: '1.2rem'}}>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <img 
                                            src = {practiceInstance.url}
                                            alt = "clinic"
                                            style = {{width: '100%', height: '100%'}}
                                            />
                                        </Col>
                                    </Row>
                                </div>
                            </Col>
                    
                            {/* Right Image */}
                            <Col xs={12} md={6}>
                                <Map address={practiceInstance.city+", "+practiceInstance.state} zm={8} />
                            </Col>

                        </Row>
                    </Row>
                    <Row>
                        <Col>
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>State Legislation</h1>
                        </Col>
                    </Row>
                    <Row>
                            {legislationData && (
                            Object.entries(legislationData).map(([key, value], index) => (
                            <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                                <PolicyInstance policy={[value.state, value, value.id]}></PolicyInstance>
                            </Col>
                            )))};
                        </Row>
                        <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
                        <Col>
                        <p>Cards Displayed: {currentPageLeg * itemsPerPage + 1} - {Math.min((currentPageLeg + 1) * itemsPerPage, totalItemLeg)} of {totalItemLeg}</p>
                        </Col>
                    </Row>
                        <Col className="d-flex justify-content-center Paragraph-style-lg">
                        <Pagination
                        currentPage={currentPageLeg}
                        totalPages={totalPagesLeg}
                        handlePageChange={handlePageChangeLeg}
                        />
                    </Col>
                        <Row>

                            <Col>
                                <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>Nearby Events</h1>
                            </Col>
                        </Row>
                        <Row >
                            {eventData && eventData.length > 0 ? 
                            (eventData.map((event, index) => (
                            <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                                <EventInstance
                                    event={event}
                                    currentDate={formattedDate}
                                />
                            </Col>
                            ))): (
                                <Col className="mt-4 Card-text-dg"style={{ textAlign: 'center', width: '100%', fontSize:'1.25rem' }}>
                                    No events
                                </Col>
                            )}
                        </Row>
                        <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
                            <Col>
                            <p>Cards Displayed: {currentPageEvent * itemsPerPage + 1} - {Math.min((currentPageEvent + 1) * itemsPerPage, totalItemEvent)} of {totalItemEvent}</p>
                            </Col>
                        </Row>
                            <Col className="d-flex justify-content-center Paragraph-style-lg">
                                <Pagination
                                    currentPage={currentPageEvent}
                                    totalPages={totalPagesEvent}
                                    handlePageChange={handlePageChangeEvent}
                                />
                        </Col>
                </Row>
            </Container>
        </div>
    );
}
export default PracticeInstancePage;