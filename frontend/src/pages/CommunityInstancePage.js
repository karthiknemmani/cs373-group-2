import { useParams, Link } from "react-router-dom";
import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarDay, faClock, faMapMarkerAlt, faUsers, faCalendarCheck, faAlignLeft } from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import Map from '../components/MapComponent';
import Connections from "../components/Connections";
import PolicyInstance from '../components/PolicyInstance';
import PracticeInstance from '../components/PracticeInstance';
import TimeConverter from '../components/ConvertTime';
import ConvertDate from "../components/ConvertDate";
import Pagination from '../components/Pagination';

function CommunityInstancePage() {
    const {communityId} = useParams();
    console.log(communityId);
    const [eventInstance, setEventInstance] = useState(null);

    const [legislationData, setLegislationData] = useState(null);
    const [clinicsData, setClinicsData] = useState(null);

    const [currentPageLeg, setCurrentPageLeg] = useState(0);
    const [currentPageClinic, setCurrentPageClinic] = useState(0);
    const [totalPagesLeg, setTotalPagesLeg] = useState(0);
    const [totalPagesClinic, setTotalPagesClinic] = useState(0);
    const [totalItemLeg, setTotalItemsLeg] = useState(0);
    const [totalItemClinic, setTotalItemsClinic] = useState(0);
    const itemsPerPage = 9;
    
    const handlePageChangeLeg = (selectedPage) => {
        setCurrentPageLeg(selectedPage.selected);
    }

    const handlePageChangeClinic = (selectedPage) => {
        setCurrentPageClinic(selectedPage.selected);
    }
    useEffect(() => {
        const fetchData = async () => {
            try {
              const response = await axios.get(`https://api.choiceconnect.me/activities?id=${communityId}`);
              console.log(response);
              console.log(response.data);
              setEventInstance(response.data);
            } catch (error) {
              console.log(error);
            }
          };
      
        fetchData();
    }, [communityId]); // Add communityId to the dependency array to re-run the effect when it changes

    useEffect(() => {
        const getModelData = async () => {
            try {
              let apiUrl = `https://api.choiceconnect.me/activities?id=${communityId}`;
              // If filtering is enabled and it's an external filter, modify the API URL
              const response = await axios.get(apiUrl);
      
              const legislationIds = response.data.legislations;
              const clinicIds = response.data.clinics;


      
              let legislationData = await Promise.all(legislationIds.map(async (id) => {
                  const legislationResponse = await axios.get(`https://api.choiceconnect.me/legislation?id=${id}`);
                  return legislationResponse.data;
              }));

              let clinicsData = await Promise.all(clinicIds.map(async (id) => {
                const clinicResponse = await axios.get(`https://api.choiceconnect.me/clinics?id=${id}`);
                return clinicResponse.data;
            }));

            setTotalItemsClinic(clinicIds.length);
            setTotalPagesClinic(Math.ceil(clinicIds.length / itemsPerPage));
            clinicsData = clinicsData.slice(currentPageClinic * itemsPerPage, (currentPageClinic + 1) * itemsPerPage);
            setClinicsData(clinicsData);

            setTotalItemsLeg(legislationIds.length);
            setTotalPagesLeg(Math.ceil(legislationIds.length / itemsPerPage));
            legislationData = legislationData.slice(currentPageLeg * itemsPerPage, (currentPageLeg + 1) * itemsPerPage);
            setLegislationData(legislationData);


            } catch (error) {
              console.log(error);
            }
          };
          getModelData();
    }, [communityId, currentPageLeg, currentPageClinic]);

    if (!eventInstance) {
        return <div>Loading...</div>; // Display loading or not found message
    }

    const attendance = (inPerson) => {
        return inPerson ? 'In Person' : 'Virtual';
    }

    // hard coded, remove when implementing api
    // const clinic = {1: 3, 2: 1, 3: 2};
    // const legislations = {1: 1, 2: 3, 3: 2}

    console.log(eventInstance)
    const time = TimeConverter(eventInstance.time);
    const date = ConvertDate(eventInstance.date);

    return (
        <div>
            <Container fluid>
                <Row className="Page-lp" style={{ paddingLeft: '15%' , paddingRight: '15%'}}>
                    <Row className="justify-content-start">
                        {/* Title */}
                        <Col xs={12} className="mt-5">
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>{eventInstance.title}</h1>
                        </Col>
                
                        {/* Content */}
                        <Row className="mt-4 Card-text-dg" style={{ textAlign: 'left' }}>
                            {/* Left Box with Text */}
                            <Col xs={12} md={6}>
                                <div className="p-4 bg-light">
                                    {/* Date with Icon */}
                                    <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faCalendarDay} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                {date}
                                            </p>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                        <p>
                                            <FontAwesomeIcon icon={faClock} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                        </p>
                                        <p>
                                            {time}
                                        </p>
                                        </Col>
                                    </Row>
                                    <Row style={{marginBottom: '1.2rem'}}>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faMapMarkerAlt} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                {eventInstance.venue}<br/>
                                                {eventInstance.address}
                                            </p>
                                        </Col>
                                    </Row>
                                    {/* <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                        <p>
                                            <FontAwesomeIcon icon={faUsers} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                        </p>
                                        <p>
                                            {attendance(eventInstance.inPerson)}
                                        </p>
                                        </Col>
                                    </Row> */}
                                    {/* <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                        <p>
                                            <FontAwesomeIcon icon={faCalendarCheck} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                        </p>
                                        <p>
                                            {eventInstance.eventType}
                                        </p>
                                        </Col>
                                    </Row> */}
                                    {/* <Row>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <p>
                                                <FontAwesomeIcon icon={faAlignLeft} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                            </p>
                                            <p>
                                                {eventInstance.description}
                                            </p>
                                        </Col>
                                    </Row> */}
                                    <Row style={{marginBottom: '1.2rem'}}>
                                        <Col style={{display: 'flex', flexDirection: 'row'}}>
                                            <img 
                                            src = {eventInstance.url}
                                            alt = "clinic"
                                            style = {{width: '100%', height: '100%'}}
                                            />
                                        </Col>
                                    </Row>
                                </div>
                            </Col>

                            {/* Right Image */}
                            <Col xs={12} md={6}>
                                <Map address={eventInstance.address+""} zm={14} />
                            </Col>
                            
                        </Row>
                    </Row>
                    <Row>
                        <Col>
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>State Legislation</h1>
                        </Col>
                    </Row>
                    <Row>
                        {legislationData && (
                            Object.entries(legislationData).map(([key, value], index) => (
                                <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                                    <PolicyInstance policy={[value.state, value, value.id]}></PolicyInstance>
                                </Col>
                            ))
                        )}
                    </Row>
                    <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
                        <Col>
                            <p>Cards Displayed: {currentPageLeg * itemsPerPage + 1} - {Math.min((currentPageLeg + 1) * itemsPerPage, totalItemLeg)} of {totalItemLeg}</p>
                        </Col>
                    </Row>
                    <Col className="d-flex justify-content-center Paragraph-style-lg">
                        <Pagination
                            currentPage={currentPageLeg}
                            totalPages={totalPagesLeg}
                            handlePageChange={handlePageChangeLeg}
                        />
                    </Col>
                    <Row>
                        <Col>
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>Nearby Clinics</h1>
                        </Col>
                    </Row>
                    <Row >
                        {clinicsData && clinicsData.length > 0 ? 
                        (clinicsData.map((practice, index) => (
                        <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                            <PracticeInstance
                                practice={practice}
                            />
                        </Col>
                        ))): (
                            <Col className="mt-4 Card-text-dg"style={{ textAlign: 'center', width: '100%', fontSize:'1.25rem' }}>
                                No clinics
                            </Col>
                        )}
                    </Row>
                    <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
                        <Col>
                        <p>Cards Displayed: {currentPageClinic * itemsPerPage + 1} - {Math.min((currentPageClinic + 1) * itemsPerPage, totalItemClinic)} of {totalItemClinic}</p>
                        </Col>
                    </Row>
                    <Col className="d-flex justify-content-center Paragraph-style-lg">
                        <Pagination
                        currentPage={currentPageClinic}
                        totalPages={totalPagesClinic}
                        handlePageChange={handlePageChangeClinic}
                        />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
export default CommunityInstancePage;