import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PureComponent } from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer, BarChart, Bar, Rectangle, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ScatterChart, Scatter, Label } from 'recharts';
import axios from 'axios';


function ProviderVisualizations() {
  const scrollableMenuStyle = {
    maxHeight: '25rem',
    overflowY: 'scroll',
  }

  const [radarData, setRadarData] = useState(null);
  const [data, setData] = useState([
    {"name": "Affiliate Center", "count": 0},
    {"name": "Comprehensive Center", "count": 0},
    {"name": "ReEntry Program Provider", "count": 0}
  ]);
  const [countyData, setCountyData] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get(`https://cs373-backend.secondchancesupport.me/api/v1/counties`);
        setCountyData(response.data);
        let crimes = {
          property: 0,
          violent: 0,
          drug: 0,
          other: 0
        };
        let total = 0;
      
        response.data.forEach(county => {
          crimes.property += county.typeproperty;
          crimes.violent += county.typeviolent;
          crimes.drug += county.typedrug;
          crimes.other += county.typeother;
        });
        Math.max(...Object.values(crimes))
        crimes = Object.keys(crimes).map(key => ({
          subject: key,
          A: crimes[key],
          fullMark: total
        }));
        console.log(crimes)
        setRadarData(crimes) 

        response = await axios.get("https://cs373-backend.secondchancesupport.me/api/v1/reentries");
        const newData = [
          {"name": "Affiliate Center", "count": 0},
          {"name": "Comprehensive Center", "count": 0},
          {"name": "ReEntry Program Provider", "count": 0}
        ];
        response.data.forEach(reentry => {
            const index = newData.findIndex(item => item.name === reentry.programtype);
            console.log(index);
            if (index !== -1) {
                newData[index]['count'] += 1;
            }
        });
        console.log(newData);
        setData(newData);


      } catch (error) {
        console.error('Error fetching data API', error);
      }
    };

    fetchData();
  }, []); // Empty dependency array to ensure useEffect runs only once

  return (
    
    <Container fluid>
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style" style={{ marginBottom: '6%' }}>Provider Visualizations</h1>
        <Row>
          <p className="Paragraph-style-dg-center">
            Count of Counties by Type of Crime
          </p>
        </Row>
        <Row style={{ marginBottom: '6%' }}>
          {radarData !== null && (
            <ResponsiveContainer width="100%" height={800}>
              <RadarChart cx="50%" cy="50%" outerRadius="80%" data={radarData}>
                <PolarGrid stroke="#21181c"/>
                <PolarAngleAxis stroke="#21181c" dataKey="subject" />
                <PolarRadiusAxis stroke="#21181c"/>
                <Radar name="Counties" dataKey="A" stroke="#d01664" fill="#d01664" fillOpacity={0.6} />
                <Legend 
                  iconType="circle" // Set the type of legend icons (e.g., 'circle', 'rect')
                />
              </RadarChart>
            </ResponsiveContainer>
          )}
        </Row>
        <Row>
          <p className="Paragraph-style-dg-center">
            Count of Centers by Type
          </p>
        </Row>
        <Container width="100%" height={800} className="d-flex justify-content-center">
          <Row style={{ marginBottom: '6%' }}>
            <BarChart
              width={800}
              height={1000}
              data={data}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5
              }}
            >
              <CartesianGrid stroke="#21181c" strokeOpacity={.4} strokeDasharray="3 3" />
              <XAxis stroke="#21181c" dataKey="name" />
              <YAxis stroke="#21181c"/>
              <Tooltip />
              <Legend 
                iconType="circle" // Set the type of legend icons (e.g., 'circle', 'rect')
              />
              <Bar dataKey="count" fill="#d01664" />
            </BarChart>
          </Row>
        </Container>
        <Row>
          <p className="Paragraph-style-dg-center">
            Population v Total Crime
          </p>
        </Row>
        <ResponsiveContainer width="100%" height={800} className="d-flex justify-content-center">
          <ScatterChart
            margin={{
              top: 20,
              right: 125,
              bottom: 50,
              left: 125,
            }}
          >
            <CartesianGrid stroke="#21181c" strokeOpacity={.4}/>
            <XAxis stroke="#21181c" type="number" dataKey="typetotal" name="Total Crime" unit="" >
            <Label stroke="#21181c" value="Total Crime" offset={0} position="outside" angle={0} dx = {0} dy = {30}/>
            </XAxis>
            <YAxis stroke="#21181c" type="number" dataKey="population" name="population" unit="">
            <Label stroke="#21181c" value="Population" offset={0} position="outside" angle={-90} dx = {-60} dy = {0}/>
            </YAxis>
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Scatter name="A school" data={countyData} fill="#d01664" />
          </ScatterChart>
        </ResponsiveContainer>
      </Row>
    </Container>
  );
}

export default ProviderVisualizations;
