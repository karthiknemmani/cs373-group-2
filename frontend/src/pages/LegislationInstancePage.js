import { useParams, Link } from "react-router-dom";
import legislationData from '../data/legislation.json';
import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faAlignLeft, faX, faFemale } from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import Connections from "../components/Connections";
import EventInstance from "../components/EventInstance";
import PracticeInstance from "../components/PracticeInstance";
import MapContainer from "../components/MapComponent";
import Pagination from "../components/Pagination";

const stateAbbreviations = {
    "Alabama": 'al',
    "Alaska": 'ak',
    "Arizona": 'az',
    "Arkansas": 'ar',
    "California": 'ca',
    "Colorado": 'co',
    "Connecticut": 'ct',
    "Delaware": 'de',
    "District of Columbia" : 'dc',
    "Florida": 'fl',
    "Georgia": 'ga',
    "Hawaii": 'hi',
    "Idaho": 'id',
    "Illinois": 'il',
    "Indiana": 'in',
    "Iowa": 'ia',
    "Kansas": 'ks',
    "Kentucky": 'ky',
    "Louisiana": 'la',
    "Maine": 'me',
    "Maryland": 'md',
    "Massachusetts": 'ma',
    "Michigan": 'mi',
    "Minnesota": 'mn',
    "Mississippi": 'ms',
    "Missouri": 'mo',
    "Montana": 'mt',
    "Nebraska": 'ne',
    "Nevada": 'nv',
    "New Hampshire": 'nh',
    "New Jersey": 'nj',
    "New Mexico": 'nm',
    "New York": 'ny',
    "North Carolina": 'nc',
    "North Dakota": 'nd',
    "Ohio": 'oh',
    "Oklahoma": 'ok',
    "Oregon": 'or',
    "Pennsylvania": 'pa',
    "Rhode Island": 'ri',
    "South Carolina": 'sc',
    "South Dakota": 'sd',
    "Tennessee": 'tn',
    "Texas": 'tx',
    "Utah": 'ut',
    "Vermont": 'vt',
    "Virginia": 'va',
    "Washington": 'wa',
    "West Virginia": 'wv',
    "Wisconsin": 'wi',
    "Wyoming": 'wy'
  };

function LegislationInstancePage() {
    const {legislationId} = useParams();
    const [stateName, setStateName] = useState("");
    const [legislationInstance, setLegislationInstance] = useState(null); // Use null for initial state

    const [eventData, setEventData] = useState(null);
    const [clinicsData, setClinicsData] = useState(null);

    const currentDate = new Date(); // Get current date
    const year = currentDate.getFullYear(); // Get the year
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Get the month and pad with zero if needed
    const day = String(currentDate.getDate()).padStart(2, '0'); // Get the day and pad with zero if needed
    const formattedDate = `${year}-${month}-${day}`; // Format the date as "YYYY-MM-DD"

    const [currentPageEvent, setCurrentPageEvent] = useState(0);
    const [currentPageClinic, setCurrentPageClinic] = useState(0);
    const [totalPagesEvent, setTotalPagesEvent] = useState(0);
    const [totalPagesClinic, setTotalPagesClinic] = useState(0);
    const [totalItemEvent, setTotalItemsEvent] = useState(0);
    const [totalItemClinic, setTotalItemsClinic] = useState(0);

    const handlePageChangeEvent = (selectedPage) => {
        setCurrentPageEvent(selectedPage.selected);
    }

    const handlePageChangeClinic = (selectedPage) => {
        setCurrentPageClinic(selectedPage.selected);
    }

    const itemsPerPage = 9;

    useEffect(() => {
        const fetchData = async () => {
            try {
              const response = await axios.get(`https://api.choiceconnect.me/legislation?id=${legislationId}`);
              console.log("response.data:")
              console.log(response.data);
              setStateName(response.data.state);
              console.log(stateName);
              console.log(response.data.state);
              setLegislationInstance(response.data);
            } catch (error) {
              console.log(error);
            }
          };
      
          fetchData();
        // if (legislation) {
        //     setLegislationInstance(legislation); // Set the found practice object
        // }
    }, [legislationId]); // Add practiceId to the dependency array to re-run the effect when it changes

    useEffect(() => {
        const getModelData = async () => {
            try {
              let apiUrl = `https://api.choiceconnect.me/legislation?id=${legislationId}`;
              // If filtering is enabled and it's an external filter, modify the API URL
              const response = await axios.get(apiUrl);
      
              const activityIds = response.data.activities;
              const clinicIds = response.data.clinics;
      
              let activitiesData = await Promise.all(activityIds.map(async (id) => {
                  const activityResponse = await axios.get(`https://api.choiceconnect.me/activities?id=${id}`);
                  return activityResponse.data;
              }));
              
              let clinicsData = await Promise.all(clinicIds.map(async (id) => {
                const clinicResponse = await axios.get(`https://api.choiceconnect.me/clinics?id=${id}`);
                return clinicResponse.data;
            }));

            setTotalItemsClinic(clinicsData.length);
            setTotalPagesClinic(Math.ceil(clinicsData.length / itemsPerPage));
            setTotalItemsEvent(activitiesData.length);
            setTotalPagesEvent(Math.ceil(activitiesData.length / itemsPerPage));


            activitiesData = activitiesData.slice(currentPageEvent * itemsPerPage, (currentPageEvent + 1) * itemsPerPage);
            clinicsData = clinicsData.slice(currentPageClinic * itemsPerPage, (currentPageClinic + 1) * itemsPerPage);

            setEventData(activitiesData);
            setClinicsData(clinicsData);
          } catch (error) {
            console.log(error);
          }
        };
        getModelData();
  }, [legislationId, currentPageEvent, currentPageClinic]);
    
    

    if (!legislationInstance) {
        return <div>Loading...</div>; // Display loading or not found message
    }

    const exceptions = (legislation) => {
        //console.log(legislation);
        const exceptions = [];
        if (legislation.exception_rape_or_incest === true) exceptions.push("rape");
        if (legislation.exception_fetal === "Lethal fetal anomaly") exceptions.push("lethal fetal anomaly");
        if (legislation.exception_fetal === "Serious fetal anomaly") exceptions.push("serious fetal anomaly");
        if (legislation.exception_life === true) exceptions.push("life-threatening conditions");
        if (legislation.exception_health === "Any") exceptions.push("any health risks");
        if (legislation.exception_health === "Physical") exceptions.push("physical health risks");
        if (legislation.exception_health === "Major Bodily Function") exceptions.push("major bodily function health risks");
        //console.log(exceptions)
        return exceptions.length > 0 ? exceptions.join(", ") : "nothing";
    };

    // hard coded, remove when implementing api
    // const events = {1: 1, 2: 3, 3: 2};
    // const clinic = {1: 3, 2: 2, 3: 1};
      

    return (
        <div>
            <Container fluid>
                <Row className="Page-lp" style={{ paddingLeft: '15%' , paddingRight: '15%'}}>
                    <Row className="justify-content-start">
                        {/* Title */}
                        <Col xs={12} className="text-center mt-5">
                            <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>{stateName}</h1>
                        </Col>
                        {/* Content */}
                        <Row className="mt-4 Card-text-dg" style={{ textAlign: 'left' }}>
                            {/* Left Box with Text */}
                            <Col xs={12} md={6}>
                                <div className="p-4 bg-light">
                                    <div>
                                        <Row>
                                            <Col style={{ display: 'flex', flexDirection: 'row' }}>
                                                <p>
                                                    <FontAwesomeIcon icon={faClock} style={{ marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem' }} />
                                                </p>
                                                <p>
                                                    Banned after {legislationInstance.banned_after_weeks_since_LMP} weeks
                                                </p>
                                            </Col>
                                        </Row>
                                        <Row style={{marginBottom: '1.2rem'}}>
                                            <Col style={{ display: 'flex', flexDirection: 'row' }}>
                                                <p>
                                                    <FontAwesomeIcon icon={faX} style={{ marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem' }} />
                                                </p>
                                                <p>
                                                    Exceptions for {exceptions(legislationInstance)}
                                                </p>
                                            </Col>
                                        </Row>
                                        <Row style={{justifyContent: 'center'}}>
                                        <Col xs={12} md={6}>
                                            <img
                                                src={require(`../assets/states/${stateAbbreviations[stateName]}.png`)}
                                                className="img-fluid"
                                                alt = "state img"
                                            />
                                        </Col>
                                        </Row>
                                        {/*Must implement this later - don't have data scraped for it yet */}
                                        {/* <Row>
                                                <Col style={{display: 'flex', flexDirection: 'row'}}>
                                                    <p>s
                                                        <FontAwesomeIcon icon={faFemale} style={{marginRight: '0.75rem', color: '#d01664', height: '1rem', width: '1rem'}}/>
                                                    </p>
                                                    <p>
                                                        {legislationInstance.popWomenReproductiveAge.toLocaleString()} women of reproductive age
                                                    </p>
                                                </Col>
                                            </Row> */}
                                    </div>
                                </div>
                            </Col>

                            {/* Right Image */}
                            <Col>
                                <MapContainer address={stateName} zm={5}/>
                            </Col>

                        </Row>
                        <Row>
                            <Col>
                                <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>Nearby Events</h1>
                            </Col>
                        </Row>
                        <Row>
                            {eventData && eventData.length > 0 ? (
                                eventData.map((community, index) => (
                                    <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                                        <EventInstance
                                            event={community}
                                            currentDate={formattedDate}
                                        />
                                    </Col>
                                ))
                            ) : (
                                <Col className="mt-4 Card-text-dg"style={{ textAlign: 'center', width: '100%', fontSize:'1.25rem' }}>
                                    No events
                                </Col>
                            )}
                        </Row>
                        <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
            <Col>
              <p>Cards Displayed: {currentPageEvent * itemsPerPage + 1} - {Math.min((currentPageEvent + 1) * itemsPerPage, totalItemEvent)} of {totalItemEvent}</p>
            </Col>
          </Row>
            <Col className="d-flex justify-content-center Paragraph-style-lg">
                <Pagination
                    currentPage={currentPageEvent}
                    totalPages={totalPagesEvent}
                    handlePageChange={handlePageChangeEvent}
                />
        </Col>
                        <Row>

                            <Col>
                                <h1 className="Title-style-dp" style={{ textAlign: 'left'}}>Nearby Clinics</h1>
                            </Col>
                        </Row>
                        <Row >
                            {clinicsData && clinicsData.length > 0 ? 
                            (clinicsData.map((practice, index) => (
                            <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                                <PracticeInstance
                                    practice={practice}
                                />
                            </Col>
                            ))): (
                                <Col className="mt-4 Card-text-dg"style={{ textAlign: 'center', width: '100%', fontSize:'1.25rem' }}>
                                    No clinics
                                </Col>
                            )}
                        </Row>
                        <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
                            <Col>
                            <p>Cards Displayed: {currentPageClinic * itemsPerPage + 1} - {Math.min((currentPageClinic + 1) * itemsPerPage, totalItemClinic)} of {totalItemClinic}</p>
                            </Col>
                        </Row>
                        <Col className="d-flex justify-content-center Paragraph-style-lg">
                        <Pagination
                        currentPage={currentPageClinic}
                        totalPages={totalPagesClinic}
                        handlePageChange={handlePageChangeClinic}
                        />
                    </Col>

                        
                       
                    </Row>
                        {/* <Row>
                            <Col>
                                <div className="Card-title-lg">
                                    <Link to={`/community/${events[legislationInstance.id]}`} className="btn btn-primary p-4 Button-dp">
                                            <h4 style={{ marginBottom: "0" }}>See Community Events</h4>
                                    </Link>
                                </div>
                            </Col>
                            <Col>
                                <div className="Card-title-lg">
                                    <Link to={`/practices/${clinic[legislationInstance.id]}`} className="btn btn-primary p-4 Button-dp">
                                            <h4 style={{ marginBottom: "0" }}>See State Legislations</h4>
                                    </Link>
                                </div>
                            </Col>
                        </Row> */}
                </Row>
            </Container>
        </div>

    );

    
}
export default LegislationInstancePage;