import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SearchBar from '../components/SearchBar';
import Dropdown from 'react-bootstrap/Dropdown';
import EventInstance from '../components/EventInstance';
import { useEffect, useState } from 'react';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import Pagination from '../components/Pagination';


const dates = [...["None","Ascending", "Descending"]];
const times = [...["None","Ascending", "Descending"]];
const titles = [...["None","Ascending", "Descending"]];
const zips = [...["None","Ascending", "Descending"]];
const states = [...new Array("None", "Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming")];

function Community() {
  const scrollableMenuStyle = {
    maxHeight: '25rem', // Adjust the height as needed
    overflowY: 'scroll'
  };

  const [eventData, setEventData] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);
  const [currFilter, setCurrFilter] = useState("None");
  const [currFilterVal, setCurrFilterVal] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const itemsPerPage = 9;

  const currentDate = new Date(); // Get current date
  const year = currentDate.getFullYear(); // Get the year
  const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Get the month and pad with zero if needed
  const day = String(currentDate.getDate()).padStart(2, '0'); // Get the day and pad with zero if needed

  const formattedDate = `${year}-${month}-${day}`; // Format the date as "YYYY-MM-DD"

  

  useEffect(() => {
    const fetchData = async () => {
      try {
        let countResponse;
        let response;
        switch(currFilter) {
          case "None":
            response = await axios.get(`https://api.choiceconnect.me/activities?page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
            break;
          case "State":
            countResponse = await axios.get(`https://api.choiceconnect.me/activities?filter_by=state&param=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/activities?filter_by=state&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Date":
            countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=date&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=date&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Time":
            countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=time&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=time&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Zip":
            countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=zip&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=zip&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Title":
            countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=title&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=title&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Search":
            countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=activities`);
            response = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=activities&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          default:
            break;
        }
        console.log(response.data);
        setTotalItems(countResponse.data.count);
        setEventData(response.data.activities);
        setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, [currentPage, currFilter, currFilterVal]);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  const handleSearch = (query) => {
    const fetchData = async () => {
      handlePageChange({selected: 0});
      if (query === "" || !query) {
        try {
          setCurrFilter("None");
          const response = await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
          setEventData(response.data.activities);
          setTotalItems(countResponse.data.count);
          setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
        } catch (error) {
          console.log(error);
        }
      } else {
        try {
          setCurrFilter("Search");
          setCurrFilterVal(query);
          const countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=activities`);
          const response = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=activities&page=${1}&num_entries=${itemsPerPage}`);
          setEventData(response.data.activities);
          setTotalItems(countResponse.data.length);
          setTotalPages(Math.ceil(countResponse.data.length / itemsPerPage));
        } catch (error) {
          console.log(error);
        }
      }
    };
    
    fetchData();
    setSearchQuery(query);
  }

  // https://127:0.0.1:8000/activities?search?query=${query}&type=activities

  return (
    <Container fluid role="community">
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style">Community Activities</h1>

        <Row className="justify-content-center" style={{marginBottom: "2rem"}}>
          <Row className="justify-content-center">
            <Col xs={12} md={12} style={{ marginBottom: '1rem' }}>
              <SearchBar data={eventData} onSearch={handleSearch} clear={searchQuery === ''}/>
            </Col>

            <Col xs={12} sm={12} md={3} className="d-flex justify-content-center flex-wrap mb-5">
              <div className="d-flex flex-column flex-md-row gap-2 w-100 justify-content-center">
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    State
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {states.map((state, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                        try {
                        setSearchQuery("");
                        handlePageChange({selected: 0});
                        await setCurrFilterVal(state);
                        if(state === "None"){
                          setCurrFilter("None");
                          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
                          const response =  await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
                          setEventData(response.data["activities"]);
                          setTotalItems(countResponse.data["count"]);
                          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          return;
                        }
                        setCurrFilter("State");
                        const countResponse = await axios.get(`https://api.choiceconnect.me/activities?filter_by=state&param=${state}`);
                        const response =  await axios.get(`https://api.choiceconnect.me/activities?filter_by=state&param=${state}&page=${1}&num_entries=${itemsPerPage}`);
                        setEventData(response.data["activities"]);
                        setTotalItems(countResponse.data["count"]);
                        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                        } catch (error) {
                        console.log(error);
                        }
                      }
                      fetchData();
                      }}>{state}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Date
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                  {dates.map((date, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                        try {
                          setSearchQuery("");
                        handlePageChange({selected: 0});
                        if(date === "None"){
                          setCurrFilter("None");
                          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
                          const response =  await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
                          setEventData(response.data["activities"]);
                          setTotalItems(countResponse.data["count"]);
                          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          return;
                        }
                        setCurrFilter("Date");
                        let ascending = (date === "Ascending") ? "True" : "False";  
                        await setCurrFilterVal(ascending);
                        const countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=date&ascending=${ascending}`);
                        const response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=date&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                        setEventData(response.data["activities"]);
                        setTotalItems(countResponse.data["count"]);
                        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                        } catch (error) {
                        console.log(error);
                        }
                      }
                      fetchData();
                      }
                    }>{date}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Time
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                  {times.map((time, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                        try {
                          setSearchQuery("");
                        handlePageChange({selected: 0});
                        if(time === "None"){
                          setCurrFilter("None");
                          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
                          const response =  await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
                          setEventData(response.data["activities"]);
                          setTotalItems(countResponse.data["count"]);
                          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          return;
                        }
                        setCurrFilter("Time");
                        let ascending = (time === "Ascending") ? "True" : "False";  
                        await setCurrFilterVal(ascending);
                        const countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=time&ascending=${ascending}`);
                        const response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=time&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                        setEventData(response.data["activities"]);
                        setTotalItems(countResponse.data["count"]);
                        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                        } catch (error) {
                        console.log(error);
                        }
                      }
                      fetchData();
                      }
                    }>{time}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Zip Codes
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                  {zips.map((zip, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                        try {
                          setSearchQuery("");
                        handlePageChange({selected: 0});
                        if(zip === "None"){
                          setCurrFilter("None");
                          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
                          const response =  await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
                          setEventData(response.data["activities"]);
                          setTotalItems(countResponse.data["count"]);
                          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          return;
                        }
                        setCurrFilter("Zip");
                        let ascending = (zip === "Ascending") ? "True" : "False";  
                        await setCurrFilterVal(ascending);
                        const countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=zip&ascending=${ascending}`);
                        const response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=zip&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                        setEventData(response.data["activities"]);
                        setTotalItems(countResponse.data["count"]);
                        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                        } catch (error) {
                        console.log(error);
                        }
                      }
                      fetchData();
                      }
                    }>{zip}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Title
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                  {titles.map((title, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                        try {
                          setSearchQuery("");
                        handlePageChange({selected: 0});
                        if(title === "None"){
                          setCurrFilter("None");
                          const countResponse = await axios.get(`https://api.choiceconnect.me/activities`);
                          const response =  await axios.get(`https://api.choiceconnect.me/activities?page=${1}&num_entries=${itemsPerPage}`);
                          setEventData(response.data["activities"]);
                          setTotalItems(countResponse.data["count"]);
                          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          return;
                        }
                        setCurrFilter("Title");
                        let ascending = (title === "Ascending") ? "True" : "False";  
                        await setCurrFilterVal(ascending);
                        const countResponse =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=title&ascending=${ascending}`);
                        const response =  await axios.get(`https://api.choiceconnect.me/activities?sort_by=title&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                        setEventData(response.data["activities"]);
                        setTotalItems(countResponse.data["count"]);
                        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                        } catch (error) {
                        console.log(error);
                        }
                      }
                      fetchData();
                      }
                    }>{title}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </Col>

          </Row>
          
          <Row style={{ display: 'flex', flexWrap: 'wrap' }}>
            {eventData && eventData.map((community, index) => (
              <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                <EventInstance
                  event={community}
                  currentDate={formattedDate}
                  searchQuery={searchQuery ? searchQuery : ""}
                />
              </Col>
            ))}
          </Row>

          <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
            <Col>
              <p>Cards Displayed: {currentPage * itemsPerPage + 1} - {Math.min((currentPage + 1) * itemsPerPage, totalItems)} of {totalItems}</p>
            </Col>
          </Row>
          <Col className="d-flex justify-content-center Paragraph-style-lg">
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              handlePageChange={handlePageChange}
            />
          </Col>
      </Row>
    </Row>
  </Container>
  );
}

export default Community;
