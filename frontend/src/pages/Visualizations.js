import React, { useEffect, useState, useCallback } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PureComponent } from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer, BarChart, Bar, Rectangle, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Cell, Sector } from 'recharts';
import axios from 'axios';


const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
    state
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`${state} ${value}`}</text>
    </g>
  );
};

function Visualizations() {
  const scrollableMenuStyle = {
    maxHeight: '25rem',
    overflowY: 'scroll',
  }

  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );
  const [radarData, setRadarData] = useState(null);
  const [clinicData, setClinicData] = useState(null);
  const [data, setData] = useState([
    {"state":"Alabama","facilities":0, "activities": 0},
    {"state":"Alaska","facilities":0, "activities": 0},
    {"state":"Arizona","facilities":0, "activities": 0},
    {"state":"Arkansas","facilities":0, "activities": 0},
    {"state":"California","facilities":0, "activities": 0},
    {"state":"Colorado","facilities":0, "activities": 0},
    {"state":"Connecticut","facilities":0, "activities": 0},
    {"state":"Delaware","facilities":0, "activities": 0},
    {"state":"District of Columbia","facilities":0, "activities": 0},
    {"state":"Florida","facilities":0, "activities": 0},
    {"state":"Georgia","facilities":0, "activities": 0},
    {"state":"Hawaii","facilities":0, "activities": 0},
    {"state":"Idaho","facilities":0, "activities": 0},
    {"state":"Illinois","facilities":0, "activities": 0},
    {"state":"Indiana","facilities":0, "activities": 0},
    {"state":"Iowa","facilities":0, "activities": 0},
    {"state":"Kansas","facilities":0, "activities": 0},
    {"state":"Kentucky","facilities":0, "activities": 0},
    {"state":"Louisiana","facilities":0, "activities": 0},
    {"state":"Maine","facilities":0, "activities": 0},
    {"state":"Maryland","facilities":0, "activities": 0},
    {"state":"Massachusetts","facilities":0, "activities": 0},
    {"state":"Michigan","facilities":0, "activities": 0},
    {"state":"Minnesota","facilities":0, "activities": 0},
    {"state":"Mississippi","facilities":0, "activities": 0},
    {"state":"Missouri","facilities":0, "activities": 0},
    {"state":"Montana","facilities":0, "activities": 0},
    {"state":"Nebraska","facilities":0, "activities": 0},
    {"state":"Nevada","facilities":0, "activities": 0},
    {"state":"New Hampshire","facilities":0, "activities": 0},
    {"state":"New Jersey","facilities":0, "activities": 0},
    {"state":"New Mexico","facilities":0, "activities": 0},
    {"state":"New York","facilities":0, "activities": 0},
    {"state":"North Carolina","facilities":0, "activities": 0},
    {"state":"North Dakota","facilities":0, "activities": 0},
    {"state":"Ohio","facilities":0, "activities": 0},
    {"state":"Oklahoma","facilities":0, "activities": 0},
    {"state":"Oregon","facilities":0, "activities": 0},
    {"state":"Pennsylvania","facilities":0, "activities": 0},
    {"state":"Rhode Island","facilities":0, "activities": 0},
    {"state":"South Carolina","facilities":0, "activities": 0},
    {"state":"South Dakota","facilities":0, "activities": 0},
    {"state":"Tennessee","facilities":0, "activities": 0},
    {"state":"Texas","facilities":0, "activities": 0},
    {"state":"Utah","facilities":0, "activities": 0},
    {"state":"Vermont","facilities":0, "activities": 0},
    {"state":"Virginia","facilities":0, "activities": 0},
    {"state":"Washington","facilities":0, "activities": 0},
    {"state":"West Virginia","facilities":0, "activities": 0},
    {"state":"Wisconsin","facilities":0, "activities": 0},
    {"state":"Wyoming","facilities":0, "activities": 0}
]);


  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get(`https://api.choiceconnect.me/legislation`);
        let exceptions = {
          fetal: 0,
          health: 0,
          life: 0,
          rape_or_incest: 0
        };
      
        response.data.legislation.forEach(state => {
          if (state.exception_fetal !== "N/A") exceptions.fetal++;
          if (state.exception_health !== "N/A") exceptions.health++;
          if (state.exception_life) exceptions.life++;
          if (state.exception_rape_or_incest) exceptions.rape_or_incest++;
        });
        exceptions = Object.keys(exceptions).map(key => ({
          subject: key.replace(/_/g, ' '), // Replace underscores with spaces
          A: exceptions[key],
          fullMark: 50
        }));
        response = await axios.get(`https://api.choiceconnect.me/clinics`);
        const clinicData = response.data.clinics;

        // Process clinicData and update data
        const newData = [...data]; // Create a copy of data
        clinicData.forEach(clinic => {
            const index = newData.findIndex(item => item.state === clinic.state);
            if (index !== -1) {
                newData[index].facilities += clinic.num_facilities;
            }
        });
        setData(newData);
        response = await axios.get(`https://api.choiceconnect.me/activities`);
        const activitiesData = response.data.activities;
        
        activitiesData.forEach(activity => {
            const index = newData.findIndex(item => item.state === activity.state);
            if (index !== -1) {
                newData[index].activities++;
            }
        });
        setData(newData);
        console.log(exceptions)
        setRadarData(exceptions) 
      } catch (error) {
        console.error('Error fetching data API', error);
      }
    };

    fetchData();
  }, []); // Empty dependency array to ensure useEffect runs only once

  return (
    <Container fluid>
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style" style={{ marginBottom: '6%' }}>Visualizations</h1>
        <Row>
          <p className="Paragraph-style-dg-center">
            Count of States by Legislative Exceptions
          </p>
        </Row>
        <Row style={{ marginBottom: '6%' }}>
          {radarData !== null && (
            <ResponsiveContainer width="100%" height={800}>
              <RadarChart cx="50%" cy="50%" outerRadius="80%" data={radarData}>
                <PolarGrid stroke="#21181c" strokeOpacity={1} />
                <PolarAngleAxis stroke="#21181c" dataKey="subject" />
                <PolarRadiusAxis stroke="#21181c"/>
                <Radar name="States" dataKey="A" stroke="#d01664" fill="#d01664" fillOpacity={0.6} />
                <Legend
                  iconType="circle" // Set the type of legend icons (e.g., 'circle', 'rect')
                />
              </RadarChart>
            </ResponsiveContainer>
          )}
        </Row>
        <Row>
          <p className="Paragraph-style-dg-center">
            Count of Facilities per State
          </p>
        </Row>
        <Container width="100%" height={800} className="d-flex justify-content-center"> {/* Center the Container */}
          <Row style={{ marginBottom: '6%' }}>
            <BarChart
              width={1000}
              height={800}
              data={data}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5
              }}
            >
              <CartesianGrid stroke="#21181c" strokeOpacity={.4} strokeDasharray="3 3" />
              <XAxis stroke="#21181c" dataKey="state" interval = {0} angle = {-45} textAnchor='end' height={100} />
              <YAxis stroke="#21181c"/>
              <Tooltip />
              <Legend
                iconType="circle" // Set the type of legend icons (e.g., 'circle', 'rect')
              />
              <Bar dataKey="facilities" fill="#d01664" />
            </BarChart>
          </Row>
        </Container>
        <Row>
          <p className="Paragraph-style-dg-center">
            Percentage/Count of Community Activities per State
          </p>
        </Row>
        <Row className="d-flex justify-content-center" style={{ marginBottom: '6%' }}>
          <PieChart width={800} height={800}>
            <Pie
              dataKey="activities"
              activeIndex={activeIndex}
              activeShape={renderActiveShape}
              isAnimationActive={true}
              data={data}
              cx="50%"
              cy="50%"
              innerRadius={60}
              outerRadius={250}
              fill="#d01664"
              label = {null}
              labelLine = {false}
              onMouseEnter={onPieEnter}
            />
            <Legend
              align="center" // Align the legend items to the center
              verticalAlign="bottom" // Align the legend vertically at the bottom
              layout="horizontal" // Display the legend items horizontally
              iconSize={20} // Set the size of the legend icons
              iconType="circle" // Set the type of legend icons (e.g., 'circle', 'rect')
              payload={[ // Provide custom data to the legend (if needed)
                { value: 'Number of Community Activities', type: 'circle', color: '#d01664' }, // Example legend item
              ]}
            />
          </PieChart>
        </Row>
      </Row>
    </Container>
  );
}

export default Visualizations;
