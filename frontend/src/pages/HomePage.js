import React, { useState } from 'react';
import { Container, Card, Button, Row, Col, Figure } from 'react-bootstrap'; // Import necessary components
import us_map from '../assets/home_page/US_map.png'
import community_lp from '../assets/icons/community_light_pink.png'
import community_dp from '../assets/icons/community_dark_pink.png'
import legislation_lp from '../assets/icons/legislation_light_pink.png'
import legislation_dp from '../assets/icons/legislation_dark_pink.png'
import practices_lp from '../assets/icons/hospital-sign_light_pink.png'
import practices_dp from '../assets/icons/hospital-sign_dark_pink.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import YoutubeEmbed from "../components/YTEmbed";

function HomePage() {

  const [communityImage, setCommunityImage] = useState(community_lp);
  const [legislationImage, setLegislationImage] = useState(legislation_lp);
  const [practicesImage, setPracticesImage] = useState(practices_lp);

  const handleCommunityHover = () => {
    setCommunityImage(community_dp);
  };

  const handleCommunityLeave = () => {
    setCommunityImage(community_lp);
  };

  const handleLegislationHover = () => {
    setLegislationImage(legislation_dp);
  };

  const handleLegislationLeave = () => {
    setLegislationImage(legislation_lp);
  };

  const handlePracticesHover = () => {
    setPracticesImage(practices_dp);
  };

  const handlePracticesLeave = () => {
    setPracticesImage(practices_lp);
  };

  const iconStyle = {
    height: '40%',
    width: '40%',
    padding: '5%',
    alignSelf: 'center'
  };

  const handleCardHover = (setFunction) => () => {
    setFunction(true);
  };

  const handleCardLeave = (setFunction) => () => {
    setFunction(false);
  };
  
  return (
    <div>
      <Container fluid>
        <Row className="Page-lp">
          <div>
            <h1 className="Header-style-left-typewriter">More than 50%</h1>
            <p className="Paragraph-style-dg Fade-in">
              of states in the United States have banned or severely restricted access to abortion since the overturning of Roe v. Wade in June 2022.
            </p>
            <Figure style={{ marginTop: '30px' }}>
              <Figure.Image
                src={us_map}
                alt="Image description"
                fluid
                className="Fade-in" // Apply the fade-in animation class here
                style={{ width: '70%', height: 'auto' }} // Adjust the width percentage as needed
                role="map"
              />
            </Figure>
          </div>

        </Row>
          <Row className="Page-dp">
            <div>
              <h1 className="Header-style-left-alternate">What we do</h1>
              <p className="Paragraph-style-lg">
                In light of the recent overturning of the Roe v. Wade case, the landscape of women's reproductive rights has been significantly altered, leading to widespread concerns. Numerous states have moved to enact stringent laws, effectively banning or severely restricting access to abortion services. This shift has left many individuals seeking support and resources to navigate these challenging circumstances.
              </p>
              <p className="Paragraph-style-lg">
                At ChoiceConnect, we stand committed to providing a comprehensive and accessible platform dedicated to addressing the evolving needs of women across America. Our mission is clear: to empower individuals with the knowledge, resources, and support necessary to make informed decisions about their reproductive health and rights.
              </p>
            </div>

            {/* Icons Row */}
            <div style={{alignContent: 'center', marginBottom: '6%'}}>
              <Row style={{marginTop: '5%', paddingLeft: '15%', paddingRight: '15%'}}>
                <Col className="text-center" md={4}>
                  <a href="/Community" style={{ textDecoration: 'none', marginBottom: '2rem' }}>
                    <Card
                      className="Card"
                      onMouseEnter={handleCardHover(handleCommunityHover)}
                      onMouseLeave={handleCardLeave(handleCommunityLeave)}
                    >
                      <Card.Img variant="top" src={communityImage} style={iconStyle} />
                      <Card.Body>
                          <Card.Title className="Card-title-lp">Community</Card.Title>
                          <Card.Text className="Card-text-lp">
                            See what's happening in your area
                          </Card.Text>
                        </Card.Body>
                    </Card>
                  </a>
                </Col>

                <Col className="text-center" md={4}>
                  <a href="/Legislation" style={{ textDecoration: 'none', marginBottom: '2rem' }}>
                    <Card
                      className="Card"
                      onMouseEnter={handleCardHover(handleLegislationHover)}
                      onMouseLeave={handleCardLeave(handleLegislationLeave)}
                    >
                      <Card.Img variant="top" src={legislationImage} style={iconStyle} />
                      <Card.Body>
                          <Card.Title className="Card-title-lp">Legislation Information</Card.Title>
                          <Card.Text className="Card-text-lp">
                            Learn how your state is affected
                          </Card.Text>
                        </Card.Body>
                    </Card>
                  </a>
                </Col>

                <Col className="text-center" md={4}>
                  <a href="/Practices" style={{ textDecoration: 'none', marginBottom: '2rem' }}>
                    <Card
                      className="Card"
                      onMouseEnter={handleCardHover(handlePracticesHover)}
                      onMouseLeave={handleCardLeave(handlePracticesLeave)}
                    >
                      <Card.Img variant="top" src={practicesImage} style={iconStyle} />
                      <Card.Body>
                          <Card.Title className="Card-title-lp">Practices & Clinics</Card.Title>
                          <Card.Text className="Card-text-lp">
                            Search nearby practices and clinics
                          </Card.Text>
                        </Card.Body>
                    </Card>
                  </a>
                </Col>
              </Row>
            </div>
            <div className="App">
              <YoutubeEmbed embedId="GhhAi0aewoU" />
            </div>
          </Row>
      </Container>
    </div>
  );
}

export default HomePage;
