import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import EventInstance from '../components/EventInstance';
import PolicyInstance from '../components/PolicyInstance';
import PracticeInstance from '../components/PracticeInstance';
import Pagination from '../components/Pagination';
import axios from 'axios';


const GlobalSearch = () => {
    const [searchParams] = useSearchParams();
    const searchQuery = searchParams.get('query');

    const [eventData, setEventData] = useState(null);
    const [legislationData, setLegislationData] = useState(null);
    const [clinicData, setClinicData] = useState(null);
    const [currentPageLeg, setCurrentPageLeg] = useState(0);
    const [currentPageEvent, setCurrentPageEvent] = useState(0);
    const [currentPageClinic, setCurrentPageClinic] = useState(0);
    const [totalPagesLeg, setTotalPagesLeg] = useState(0);
    const [totalPagesEvent, setTotalPagesEvent] = useState(0);
    const [totalPagesClinic, setTotalPagesClinic] = useState(0);
    const [totalItemLeg, setTotalItemsLeg] = useState(0);
    const [totalItemEvent, setTotalItemsEvent] = useState(0);
    const [totalItemClinic, setTotalItemsClinic] = useState(0);
    const itemsPerPage = 9;

    const handlePageChangeLeg = (selectedPage) => {
        setCurrentPageLeg(selectedPage.selected);
    }

    const handlePageChangeEvent = (selectedPage) => {
        setCurrentPageEvent(selectedPage.selected);
    }

    const handlePageChangeClinic = (selectedPage) => {
        setCurrentPageClinic(selectedPage.selected);
    }

    
    console.log(searchQuery)

    useEffect(() => {
        // Reset state before fetching new data
        setEventData(null);
        setLegislationData(null);
        setClinicData(null);

        const fetchData = async () => {
            try {
                let countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=activities`);
                let response = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=activities&page=${currentPageEvent + 1}&num_entries=${itemsPerPage}`);
                setEventData(response.data.activities);
                setTotalItemsEvent(countResponse.data['count']);
                setTotalPagesEvent(Math.ceil(countResponse.data['count'] / itemsPerPage));
                countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=legislation`);
                response = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=legislation&page=${currentPageLeg + 1}&num_entries=${itemsPerPage}`);
                setLegislationData(response.data.legislation);
                setTotalItemsLeg(countResponse.data['count']);
                setTotalPagesLeg(Math.ceil(countResponse.data['count'] / itemsPerPage));
                countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=clinics`);
                response = await axios.get(`https://api.choiceconnect.me/search?query=${searchQuery}&type=clinics&page=${currentPageClinic + 1}&num_entries=${itemsPerPage}`);
                setClinicData(response.data.clinics);
                setTotalItemsClinic(countResponse.data['count']);
                setTotalPagesClinic(Math.ceil(countResponse.data["count"] / itemsPerPage));
            } catch (error) {
                console.error('Error fetching event data: ', error);
            }
        };

        fetchData();
    },[currentPageLeg, currentPageEvent, currentPageClinic, searchQuery]);

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;

    return (
        searchQuery ? (

        <div>
        <Container fluid role="community" style={{ marginBottom: '-4rem' }}>
        <Row className="Page-lp justify-content-center">
            <h1 className="Header-style">Community Activities</h1>
            <Row style={{ display: 'flex', flexWrap: 'wrap' }}>
                {eventData && eventData.map((community, index) => (
                <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                    <EventInstance
                    event={community}
                    currentDate={formattedDate}
                    searchQuery={searchQuery ? searchQuery : ""}
                    />
                </Col>
                ))}
            </Row>
            <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
            <Col>
              <p>Cards Displayed: {currentPageEvent * itemsPerPage + 1} - {Math.min((currentPageEvent + 1) * itemsPerPage, totalItemEvent)} of {totalItemEvent}</p>
            </Col>
          </Row>
            <Col className="d-flex justify-content-center Paragraph-style-lg">
          <Pagination
            currentPage={currentPageEvent}
            totalPages={totalPagesEvent}
            handlePageChange={handlePageChangeEvent}
          />
        </Col>
        </Row>

        </Container>

        <Container fluid role="legislation" style={{ marginBottom: '-4rem' }}>
          <Row className="Page-lp justify-content-center">
            <h1 className="Header-style">Legislations</h1>
            <Row>
              {legislationData && (
              Object.entries(legislationData).map(([key, value], index) => (
              <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                  <PolicyInstance policy={[value.state, value, value.id]} searchQuery={searchQuery}></PolicyInstance>
              </Col>
              )))};
            </Row>

            <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
              <Col>
                <p>Cards Displayed: {currentPageLeg * itemsPerPage + 1} - {Math.min((currentPageLeg + 1) * itemsPerPage, totalItemLeg)} of {totalItemLeg}</p>
              </Col>
            </Row>

            <Col className="d-flex justify-content-center Paragraph-style-lg">
              <Pagination
              currentPage={currentPageLeg}
              totalPages={totalPagesLeg}
              handlePageChange={handlePageChangeLeg}
              />
            </Col>

          </Row>
        </Container>

        <Container fluid role="legislation" style={{ marginBottom: '-4rem' }}>
          <Row className="Page-lp justify-content-center">
            <h1 className="Header-style">Cities</h1>
            <Row>
              {clinicData && (
                  Object.entries(clinicData).map(([key, value], index) => (
                    // console.log(key, value, index),
                  <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                    <PracticeInstance practice={value} searchQuery={searchQuery ? searchQuery : ""}
                    ></PracticeInstance>
                  </Col>
              )))};
            </Row>

            <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
              <Col>
              <p>Cards Displayed: {currentPageClinic * itemsPerPage + 1} - {Math.min((currentPageClinic + 1) * itemsPerPage, totalItemClinic)} of {totalItemClinic}</p>
              </Col>
            </Row>

            <Col className="d-flex justify-content-center Paragraph-style-lg">
            <Pagination
            currentPage={currentPageClinic}
            totalPages={totalPagesClinic}
            handlePageChange={handlePageChangeClinic}
            />
              </Col>

        </Row>
        </Container>
        </div> 
        
        
        
        
        ) : (
            <p>Provide valid search query.</p>
        )
    );
};

export default GlobalSearch;