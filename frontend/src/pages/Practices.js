import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SearchBar from '../components/SearchBar';
import PracticeInstance from '../components/PracticeInstance';
import Dropdown from 'react-bootstrap/Dropdown';
import Pagination from '../components/Pagination';
import axios from 'axios';

const states = ["None", "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"];
const facilities = [...["None","Ascending", "Descending"]];
const distances = [...["None","Ascending", "Descending"]];
const times = [...["None","Ascending", "Descending"]];
const women = [...["None","Ascending", "Descending"]];

function Practices() {
  const scrollableMenuStyle = {
    maxHeight: '25rem',
    overflowY: 'scroll',
  };

  const [practiceData, setPracticeData] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);
  const [currFilter, setCurrFilter] = useState("None");
  const [currFilterVal, setCurrFilterVal] = useState("");
  const[searchQuery, setSearchQuery] = useState("");
  const itemsPerPage = 9;


  useEffect(() => {
    const fetchData = async () => {
      try {
        let countResponse;
        let response;
        switch(currFilter) {
          case "None":
            response = await axios.get(`https://api.choiceconnect.me/clinics?page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
            break;
          case "State":
            response = await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse = await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${currFilterVal}`);
            break;
          case "Facility":
            countResponse = await axios.get(`https://api.choiceconnect.me/clinics?sort_by=num_facilities&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=num_facilities&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Distance":
            response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_distance&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_distance&ascending=${currFilterVal}`);
            break;
          case "Time":
            response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_time&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_time&ascending=${currFilterVal}`);
            break;
          case "Women":
            countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=total_females&ascending=${currFilterVal}`);
            response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=total_females&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Search":
            countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=clinics`);
            response = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=clinics&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          default:
            break;
        }
        setTotalItems(countResponse.data.count);
        // console.log(response.data.clinics);
        // const data = Object.values(response.data.clinics); // Convert API response to array
        setPracticeData(response.data.clinics);
        // Assuming the total number of items can be derived from the API response length
        setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, [currentPage]);


  const handlePageChange = ({ selected }) => {
    console.log(currentPage);
    console.log("switching to", selected);
    setCurrentPage(selected);
  };

  const handleSearch = (query) => {
    const fetchData = async () => {
      handlePageChange({selected: 0});
      if (query === "" || !query) {
        try {
          setCurrFilter("None");
          const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
          const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
          setPracticeData(response.data.clinics);
          setTotalItems(countResponse.data.count);
          setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
        } catch (error) {
          console.error(error);
        }
      } else {
      try {
        setCurrFilter("Search");
        setCurrFilterVal(query);
        const countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=clinics`);
        const response = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=clinics&page=${1}&num_entries=${itemsPerPage}`);
        setPracticeData(response.data.clinics);
        setTotalItems(countResponse.data.count);
        setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
      } catch (error) {
        console.error(error);
      }
    }
    }
    fetchData();
    setSearchQuery(query);
  }

  // https://127:0.0.1:8000/activities?search?query=${query}&type=clinics

  return (
    <Container fluid>
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style">Cities</h1>

        <Row className="justify-content-center" style={{ marginBottom: "2rem" }}>
          <Row className="justify-content-center">
            <Col xs={12} md={12} style={{ marginBottom: '1rem' }}>
              <SearchBar data={practiceData} onSearch={handleSearch } clear={searchQuery === ''} />
            </Col>

            <Col xs={12} sm={12} md={12} lg={12} className="d-flex justify-content-center flex-wrap mb-5">
              <div className="d-flex flex-column flex-lg-row gap-2 w-100 justify-content-center">
                <Dropdown className="flex-grow-1 flex-lg-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    State
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {states.map((state, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            // setCurrentPage(0);
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            await setCurrFilterVal(state);
                            if(state === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
                              const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
                              setPracticeData(response.data.clinics);
                              setTotalItems(countResponse.data.count);
                              setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
                              return;
                            }
                            setCurrFilter("State");
                            const countResponse = await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${state}`);
                            const response =  await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${state}&page=${1}&num_entries=${itemsPerPage}`);
                            // console.log(response);
                            setPracticeData(response.data.clinics);
                            setTotalItems(countResponse.data.count);
                            setTotalPages(Math.ceil(countResponse.data.count / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>{state}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-lg-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Number of Facilities
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {facilities.map((facility, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(facility === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
                              const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
                              setPracticeData(response.data["clinics"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Facility");
                            let ascending = (facility === "Ascending") ? "True" : "False";  
                            await setCurrFilterVal(ascending);
                            const countResponse = await axios.get(`https://api.choiceconnect.me/clinics?sort_by=num_facilities&ascending=${ascending}`);
                            const response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=num_facilities&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                            setPracticeData(response.data["clinics"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }
                    }>{facility}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-lg-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Average Distance to a Facility
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {distances.map((distance, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(distance === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
                              const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
                              setPracticeData(response.data["clinics"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Distance");
                            let ascending = (distance === "Ascending") ? "True" : "False";  
                            await setCurrFilterVal(ascending);
                            const response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_distance&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                            const countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_distance&ascending=${ascending}`);
                            setPracticeData(response.data["clinics"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }
                    }>{distance}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-lg-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Average Time to a Facility
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {times.map((time, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(time === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
                              const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
                              setPracticeData(response.data["clinics"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Time");
                            let ascending = (time === "Ascending") ? "True" : "False"; 
                            await setCurrFilterVal(ascending); 
                            const response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_time&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                            const countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=travel_time&ascending=${ascending}`);
                            setPracticeData(response.data["clinics"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }
                    }>{time}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-lg-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Women of Reproductive Age
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {women.map((wom, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(wom === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/clinics`);
                              const response =  await axios.get(`https://api.choiceconnect.me/clinics?page=${1}&num_entries=${itemsPerPage}`);
                              setPracticeData(response.data["clinics"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Women");
                            let ascending = (wom === "Ascending") ? "True" : "False";  
                            await setCurrFilterVal(ascending); 
                            const countResponse =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=total_females&ascending=${ascending}`);
                            const response =  await axios.get(`https://api.choiceconnect.me/clinics?sort_by=total_females&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                            setPracticeData(response.data["clinics"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }
                    }>{wom}</Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </Col>
          </Row>
          
          <Row>
            {practiceData && (
                Object.entries(practiceData).map(([key, value], index) => (
                  // console.log(key, value, index),
                <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                  <PracticeInstance practice={value} searchQuery={searchQuery ? searchQuery : ""}
                  ></PracticeInstance>
                </Col>
            )))};
          </Row>

          <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
            <Col>
              <p>Cards Displayed: {Math.max((currentPage) * itemsPerPage + 1, 1)} - {Math.min((currentPage + 1) * itemsPerPage, totalItems)} of {totalItems}</p>
            </Col>
          </Row>
          
          <Col className="d-flex justify-content-center Paragraph-style-lg">
          <Pagination
            currentPage={currentPage}
            totalPages={totalPages}
            handlePageChange={handlePageChange}
          />
        </Col>
        </Row>
      </Row>
    </Container>
  );
}

export default Practices;
