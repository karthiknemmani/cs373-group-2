import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SearchBar from '../components/SearchBar';
import PolicyInstance from '../components/PolicyInstance';
import Dropdown from 'react-bootstrap/Dropdown';
import legislationData from '../data/legislation.json';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import Pagination from '../components/Pagination';

const states = [...["None","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]];
const weeks = [...["None","Ascending", "Descending"]];
const life = [...["None","True", "False"]];
const health = [...["None","N/A", "Physical", "Any"]];
const fetal = [...["None","N/A", "Lethal Fetal Anomaly"]];
const rape_incest = [...["None","True", "False"]];

function Legislation() {
  const scrollableMenuStyle = {
    maxHeight: '25rem', // Adjust the height as needed
    overflowY: 'scroll'
  };
  const [legData, setLegData] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItem, setTotalItems] = useState(0);
  const [currFilter, setCurrFilter] = useState("None");
  const [currFilterVal, setCurrFilterVal] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const itemsPerPage = 9;

  useEffect(() => {
    const fetchData = async () => {
      try {
        let countResponse;
        let response;
        switch(currFilter) {
          case "None":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "State":
            response = await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            countResponse = await axios.get(`https://api.choiceconnect.me/clinics?filter_by=state&param=${currFilterVal}`);
            break;
          case "LMP":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation?sort_by=banned_after_weeks_since_LMP&ascending=${currFilterVal}`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?sort_by=banned_after_weeks_since_LMP&ascending=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Mother":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_health&param=${currFilterVal}`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_health&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Life":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_life&param=${currFilterVal}`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_life&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Rape":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_rape_or_incest&param=${currFilterVal}`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_rape_or_incest&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Fetal":
            countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_fetal&param=${currFilterVal}`);
            response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_fetal&param=${currFilterVal}&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          case "Search":
            countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=legislation`);
            response = await axios.get(`https://api.choiceconnect.me/search?query=${currFilterVal}&type=legislation&page=${currentPage + 1}&num_entries=${itemsPerPage}`);
            break;
          default:
            break;
        }

        setLegData(response.data["legislation"]);
        setTotalItems(countResponse.data["count"]);
        setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, [currentPage]);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  const handleSearch = (query) => {
    const fetchData = async () => {
      handlePageChange({selected: 0});
      if(query === "" || !query) {
        try {
          setCurrFilter("None");
          const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
          const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
          setLegData(response.data["legislation"]);
          setTotalItems(countResponse.data["count"]);
          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
        } catch (error) {
          console.log(error);
        }
      } else {
        try {
          setCurrFilter("Search");
          setCurrFilterVal(query);
          const countResponse = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=legislation`);
          const response = await axios.get(`https://api.choiceconnect.me/search?query=${query}&type=legislation&page=${1}&num_entries=${itemsPerPage}`);
          setLegData(response.data["legislation"]);
          setTotalItems(countResponse.data["count"]);
          setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
        } catch (error) {
          console.log(error);
        }
      }
    }
    fetchData();
    setSearchQuery(query);
  }

  return (
    <Container fluid>
      <Row className="Page-lp justify-content-center">
        <h1 className="Header-style">Legislation</h1>

        <Row className="justify-content-center" style={{marginBottom: "2rem"}}>
          <Row className="d-flex justify-content-center flex-wrap mb-5">
            <Col xs={12} md={12} style={{ marginBottom: '1rem' }}>
              <SearchBar onSearch={handleSearch} clear={searchQuery === ''}/>
            </Col>

            <Col xs={12} sm={12} md={12} lg={12} className="flex-wrap mb-5">
              <div className="d-flex flex-column flex-lg-row gap-2 w-100 justify-content-center">
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    State
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {states.map((state, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            handlePageChange({selected: 0});
                            await setCurrFilterVal(state);
                            setSearchQuery("");
                            if(state === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("State");
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=state&param=${state}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=state&param=${state}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {state}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Weeks after LMP
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {weeks.map((week, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(week === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("LMP");
                            let ascending = (week === "Ascending") ? "True" : "False";
                            await setCurrFilterVal(ascending);
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?sort_by=banned_after_weeks_since_LMP&ascending=${ascending}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?sort_by=banned_after_weeks_since_LMP&ascending=${ascending}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {week}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Mother's Health Exception
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {health.map((h, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            setCurrFilterVal(h);
                            if(h === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Mother");
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_health&param=${h}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_health&param=${h}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {h}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Life Exception
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {life.map((exception, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(exception === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Life");
                            console.log(exception);
                            const life = (exception === "True") ? "true" : "false";
                            setCurrFilterVal(life);
                            console.log(life);
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_life&param=${life}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_life&param=${life}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {exception}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Rape or Incest Exception
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {rape_incest.map((exception, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(exception === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(response.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Rape");
                            const rape = (exception === "True") ? "true" : "false";
                            setCurrFilterVal(rape);
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_rape_or_incest&param=${rape}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_rape_or_incest&param=${rape}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {exception}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
                <Dropdown className="flex-grow-1 flex-md-grow-0">
                  <Dropdown.Toggle className="Button-dp" id="dropdown-basic">
                    Fetal Health Exception
                  </Dropdown.Toggle>
                  <Dropdown.Menu style={scrollableMenuStyle}>
                    {fetal.map((exception, index) => (
                      <Dropdown.Item key={index} onClick={() => {
                        const fetchData = async () => {
                          try {
                            setSearchQuery("");
                            handlePageChange({selected: 0});
                            if(exception === "None"){
                              setCurrFilter("None");
                              const countResponse = await axios.get(`https://api.choiceconnect.me/legislation`);
                              const response = await axios.get(`https://api.choiceconnect.me/legislation?page=${1}&num_entries=${itemsPerPage}`);
                              setLegData(response.data["legislation"]);
                              setTotalItems(countResponse.data["count"]);
                              setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                              return;
                            }
                            setCurrFilter("Fetal");
                            const f = (exception === "N/A") ? "N/A" : "Lethal Fetal Anomaly";
                            setCurrFilterVal(f);
                            const countResponse = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_fetal&param=${f}`);
                            const response = await axios.get(`https://api.choiceconnect.me/legislation?filter_by=exception_fetal&param=${f}&page=${1}&num_entries=${itemsPerPage}`);
                            setLegData(response.data["legislation"]);
                            setTotalItems(countResponse.data["count"]);
                            setTotalPages(Math.ceil(countResponse.data["count"] / itemsPerPage));
                          } catch (error) {
                            console.log(error);
                          }
                        }
                        fetchData();
                      }}>
                        {exception}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </Col>
          </Row>
          
          <Row>
            {legData && (
              Object.entries(legData).map(([key, value], index) => (
              <Col key={index} xs={12} md={4} style={{ marginBottom: '1rem' }}>
                <PolicyInstance policy={[value.state, value, value.id]} searchQuery={searchQuery}></PolicyInstance>
              </Col>
            )))};
          </Row>

          <Row style={{fontFamily: 'Poppins', marginBottom: '1rem'}}>
            <Col>
              <p>Cards Displayed: {currentPage * itemsPerPage + 1} - {Math.min((currentPage + 1) * itemsPerPage, totalItem)} of {totalItem}</p>
            </Col>
          </Row>
        </Row>
        <Col className="d-flex justify-content-center Paragraph-style-lg">
          <Pagination
            currentPage={currentPage}
            totalPages={totalPages}
            handlePageChange={handlePageChange}
          />
        </Col>
      </Row>
    </Container>
  );
}

export default Legislation;
