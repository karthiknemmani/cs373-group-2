import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App.js'
import NBar from './components/NavBar.js'
import Community from './pages/Community.js';
import Legislation from './pages/Legislation.js';
import Practices from './pages/Practices.js';
import About from './pages/About.js';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';

jest.mock('axios');

  test('renders ChoiceConnect navbar', () => {
    render(<App />);
    const aboutButton = screen.getByRole('navigation');
    expect(aboutButton).toBeInTheDocument();
  });

  test('renders about tab within navbar', () => {
    render(
      <BrowserRouter>
        <NBar />
    </BrowserRouter>
    
      );
    const aboutButton = screen.getByText(/About/i);
    expect(aboutButton).toBeInTheDocument();
  });

  test('renders community tab within navbar', () => {
    render(
      <BrowserRouter>
      <NBar />
  </BrowserRouter>
    );
    const aboutButton = screen.getByText(/Community Activities/i);
    expect(aboutButton).toBeInTheDocument();
  });

  test('renders legislation tab within navbar', () => {
    render(
      <BrowserRouter>
      <NBar />
  </BrowserRouter>
    );
    const aboutButton = screen.getByText(/Legislation Information/i);
    expect(aboutButton).toBeInTheDocument();
  });

  test('renders practices tab within navbar', () => {
    render(
      <BrowserRouter>
      <NBar />
  </BrowserRouter>
    );
    const aboutButton = screen.getByText(/Cities/i);
    expect(aboutButton).toBeInTheDocument();
  });

  test('renders map image', () => {
    render(<App />);
    const mapElement = screen.getByRole("map");
    expect(mapElement).toBeInTheDocument();
  });

  test('renders paragraph test', () => {
    render(<App />);
    const paragraphElement = screen.getByText(/What we do/i);
    expect(paragraphElement).toBeInTheDocument();
  });

  test('renders community card', () => {
    render(<App />);
    const communityElement = screen.getByText(/See what's happening/i);
    expect(communityElement).toBeInTheDocument();
  });

  test('renders legislation card', () => {
    render(<App />);
    const legislationElement = screen.getByText(/Learn how your state/i);
    expect(legislationElement).toBeInTheDocument();
  });

  test('renders practices card', () => {
    render(<App />);
    const practicesElement = screen.getByText(/Search nearby/i);
    expect(practicesElement).toBeInTheDocument();
  });


