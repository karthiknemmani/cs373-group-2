import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';

import NBar from './components/NavBar';
import Home from './pages/HomePage';
// import About from './pages/About';
// import Community from './pages/Community';
// import Legislation from './pages/Legislation';
// import Practices from './pages/Practices';

// import LegislationInstancePage from './pages/LegislationInstancePage';
// import CommunityInstancePage from './pages/CommunityInstancePage';
// import PracticeInstancePage from './pages/PracticeInstancePage';
// import Undefined from './pages/Undefined';

const LazyAbout = React.lazy(() => import('./pages/About'));
const LazyCommunity = React.lazy(() => import('./pages/Community'));
const LazyLegislation = React.lazy(() => import('./pages/Legislation'));
const LazyPractices = React.lazy(() => import('./pages/Practices'));
const LazyLegislationInstancePage = React.lazy(() => import('./pages/LegislationInstancePage'));
const LazyCommunityInstancePage = React.lazy(() => import('./pages/CommunityInstancePage'));
const LazyPracticeInstancePage = React.lazy(() => import('./pages/PracticeInstancePage'));
const LazySearch = React.lazy(() => import('./pages/GlobalSearch'));
const LazyVisualizations = React.lazy(() => import('./pages/Visualizations'));
const LazyProvVisualizations = React.lazy(() => import('./pages/ProviderVisualizations'));
const LazyCritiques = React.lazy(() => import('./pages/Critiques'));



function App() {
  return (
    <div>
      <BrowserRouter>
        <NBar/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<React.Suspense fallback={<div>Loading...</div>}><LazyAbout /></React.Suspense>} />
          <Route path="/community" element={<React.Suspense fallback={<div>Loading...</div>}><LazyCommunity /></React.Suspense>} />
          <Route path="/legislation" element={<React.Suspense fallback={<div>Loading...</div>}><LazyLegislation /></React.Suspense>} />
          <Route path="/practices" element={<React.Suspense fallback={<div>Loading...</div>}><LazyPractices /></React.Suspense>} />
          <Route path="/legislation/:legislationId" element={<React.Suspense fallback={<div>Loading...</div>}><LazyLegislationInstancePage /></React.Suspense>} />
          <Route path="/community/:communityId" element={<React.Suspense fallback={<div>Loading...</div>}><LazyCommunityInstancePage /></React.Suspense>} />
          <Route path="/practices/:practiceId" element={<React.Suspense fallback={<div>Loading...</div>}><LazyPracticeInstancePage /></React.Suspense>} />
          <Route path="/search" element={<React.Suspense fallback={<div>Loading...</div>}><LazySearch /></React.Suspense>} />
          <Route path="/visualizations" element={<React.Suspense fallback={<div>Loading...</div>}><LazyVisualizations /></React.Suspense>} />
          <Route path="/prov-visualizations" element={<React.Suspense fallback={<div>Loading...</div>}><LazyProvVisualizations /></React.Suspense>} />
          <Route path="/critiques" element={<React.Suspense fallback={<div>Loading...</div>}><LazyCritiques /></React.Suspense>} />
        </Routes>
      </BrowserRouter>
    </div>
  );

}

export default App;
