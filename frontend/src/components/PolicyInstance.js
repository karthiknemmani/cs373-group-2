import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Highlighter from "react-highlight-words";


const stateAbbreviations = {
  "Alabama": 'al',
  "Alaska": 'ak',
  "Arizona": 'az',
  "Arkansas": 'ar',
  "California": 'ca',
  "Colorado": 'co',
  "Connecticut": 'ct',
  "Delaware": 'de',
  "District of Columbia" : 'dc',
  "Florida": 'fl',
  "Georgia": 'ga',
  "Hawaii": 'hi',
  "Idaho": 'id',
  "Illinois": 'il',
  "Indiana": 'in',
  "Iowa": 'ia',
  "Kansas": 'ks',
  "Kentucky": 'ky',
  "Louisiana": 'la',
  "Maine": 'me',
  "Maryland": 'md',
  "Massachusetts": 'ma',
  "Michigan": 'mi',
  "Minnesota": 'mn',
  "Mississippi": 'ms',
  "Missouri": 'mo',
  "Montana": 'mt',
  "Nebraska": 'ne',
  "Nevada": 'nv',
  "New Hampshire": 'nh',
  "New Jersey": 'nj',
  "New Mexico": 'nm',
  "New York": 'ny',
  "North Carolina": 'nc',
  "North Dakota": 'nd',
  "Ohio": 'oh',
  "Oklahoma": 'ok',
  "Oregon": 'or',
  "Pennsylvania": 'pa',
  "Rhode Island": 'ri',
  "South Carolina": 'sc',
  "South Dakota": 'sd',
  "Tennessee": 'tn',
  "Texas": 'tx',
  "Utah": 'ut',
  "Vermont": 'vt',
  "Virginia": 'va',
  "Washington": 'wa',
  "West Virginia": 'wv',
  "Wisconsin": 'wi',
  "Wyoming": 'wy'
};


function PolicyInstance({policy, searchQuery}) {
  // console.log(policy);
  const imgStyle={
    height: '18rem',
    objectFit: 'cover',
    width: '100%',
    };
    let key = policy[0]
    console.log("key")
    console.log(key)
    console.log(stateAbbreviations[key])
    let value = policy[1]
    // console.log("value")
    console.log(value)
    let index = policy[2]
    // for( var i in value){
    //   if(value[i] === null){
    //     value[i] = "N/A"
    //   } else if (value[i] === true){
    //     value[i] = "Yes"
    //   } else if (value[i] === false){
    //     value[i] = "No"
    //   }
    // }
    let searchArray = searchQuery ? searchQuery.split(" ") : [];
  return (
    <Link to={`/legislation/${value.id}`} style={{ textDecoration: 'none' }} role="policy">
      <Card className="Card-lg">
        <Card.Img variant="top" src={require(`../assets/states/${stateAbbreviations[key]}.png`)} style={imgStyle}/>
        <Card.Body>
          <Card.Title className="Card-text-dg" style={{ marginBottom: '5%' }}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={key}
            />
          </Card.Title>
          <Card.Subtitle className="Card-text-dg mb-2 text-muted">{value['Last Updated']}</Card.Subtitle>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px'}}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={`Number of Weeks Before Abortion Banned: ${value['banned_after_weeks_since_LMP']}`}
            />
          </Card.Text>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={`Exception for Danger to Mother's Life: ${value['exception_life'].toString()}`}
            />
          </Card.Text>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={`Exception for Risk to Mother's Health: ${value['exception_health']}`}
            />
          </Card.Text>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={`Exception for Lethal Anomaly in Fetus: ${value['exception_fetal']}`}
            />
          </Card.Text>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={searchArray}
              autoEscape={true}
              textToHighlight={`Exception for Rape/Incest: ${value['exception_rape_or_incest']}`}
            />
          </Card.Text>
        </Card.Body>
      </Card>
    </Link>
  );
};

export default PolicyInstance;