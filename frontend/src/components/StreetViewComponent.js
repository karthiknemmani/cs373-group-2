import React, { useEffect, useState } from 'react';
import { GoogleMap, useLoadScript, MarkerF, StreetViewPanorama } from '@react-google-maps/api';
import { setKey, setDefaults, fromAddress } from "react-geocode";

const libraries = ['places'];
const API_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

const StreetView = ({ address, ht = '50vh', zm }) => {

    console.log(address);
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: API_KEY,
    libraries,
  });
  const mapContainerStyle = {
    width: '100%',
    height: ht,
    marginTop: '1rem',
    marginBottom: '1rem'
  };

  const [lat, setLat] = useState(null);
  const [lng, setLng] = useState(null);

  useEffect(() => {
    setDefaults({
      key: API_KEY,
      language: "en",
    });
    fromAddress(`${address}`)
      .then(({ results }) => {
        console.log(results[0].geometry.location);
        setLat(results[0].geometry.location.lat);
        setLng(results[0].geometry.location.lng);
      })
      .catch(console.error);
  }, [address]);

  if (loadError) {
    return <div>Error loading maps</div>;
  }

  if (!isLoaded) {
    return <div>Loading maps</div>;
  }

  return (
    <div>
      <GoogleMap
        mapContainerStyle={mapContainerStyle}
        zoom={zm}
        center={{ lat: lat, lng: lng }}
         >
              {/* Display the Marker */}
              <MarkerF position={{ lat: lat, lng: lng }} />
      
              {/* Display the Street View Panorama */}
              {lat && lng && (
                <StreetViewPanorama
                  position={{ lat: lat, lng: lng }}
                  pov={{ heading: 100, pitch: 0 }}
                  motionTracking={false}
                  visible={true}
                />
              )}
            </GoogleMap>
          </div>
        );
      };
      
      export default StreetView;
      