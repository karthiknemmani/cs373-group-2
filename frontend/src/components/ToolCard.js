import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function ToolCard({ name, img }) {
  return (
    <Card className="Card" style={{ width: '12rem', height: '100%', display: 'flex', flexDirection: 'column' }}>
      <div style={{ flex: '1' }}>
        <Card.Img variant="top" src={img} style={{ objectFit: 'cover', height: '100%' }} />
      </div>
      <Card.Body className="d-flex flex-column justify-content-end p-2">
        <Card.Title className="Card-text-lp mb-0">{name}</Card.Title>
      </Card.Body>
    </Card>
  );
}

export default ToolCard;