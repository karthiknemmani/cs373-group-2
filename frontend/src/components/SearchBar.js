import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const SearchBar = ({ data, onSearch, global = false, clear }) => {
  const [query, setQuery] = useState('');

  const handleChange = (event) => {
    const value = event.target.value;
    setQuery(value);
  };

  const handleSearch = (event) => {
    event.preventDefault(); // Prevent default form submission behavior
    onSearch(query);
  };

  useEffect(() => {
    if (clear) {
      setQuery('');
    }
  }, [clear])

  return (
    global ? (
      <Form onSubmit={handleSearch}>
        <div className="input-group">
          <Form.Control type="search" placeholder="Search" onChange={handleChange} value={query} />
          <Link to={`/search?query=${query}`} style={{ backgroundColor: '#d01664', borderColor: '#d01664' }}>
            <Button variant="primary" onClick={handleChange} style={{ backgroundColor: '#d01664' , borderColor: '#d01664' }}>
              <FontAwesomeIcon icon={faSearch} style={{ height: '1rem', width: '1rem'}} />
            </Button>
          </Link>
        </div>
      </Form>
    ) : (
      <Form onSubmit={handleSearch}> {/* Use onSubmit event handler */}
        <div className="input-group">
          <Form.Control type="search" placeholder="Search" onChange={handleChange} value={query} />
          <Button variant="primary" onClick={handleSearch} style={{ backgroundColor: '#d01664' , borderColor: '#d01664' }}>
            <FontAwesomeIcon icon={faSearch} style={{ height: '1rem', width: '1rem'}} />
          </Button>

        </div>
      </Form>
    )
  );
};

export default SearchBar;
