// returns proper path based on whether clinics/events are there
const Connections = (model, arr) => {
    console.log(typeof arr);
    if (arr === undefined || arr.length === 0) {
        return 'undefined';
    } else {
        return model + '/' + arr[0];
    }
}

export default Connections;