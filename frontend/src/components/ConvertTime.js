

function TimeConverter(prevTime) {
    // Split the military time into hours, minutes, and seconds

    const [startTime, endTime] = prevTime.split('-').map(time => time.trim());

    const convertIndividualTime = (time) => {
        const [hours, minutes, seconds] = time.split(':');

        // Convert hours to 12-hour format
        let convertedHours = parseInt(hours, 10) % 12;
        if (convertedHours === 0) {
            convertedHours = 12;
        }

        // Determine if it's AM or PM
        const period = parseInt(hours, 10) < 12 ? 'AM' : 'PM';

        // Return the converted time in normal format
        return `${convertedHours}:${minutes} ${period}`;
    }

    return `${convertIndividualTime(startTime)} - ${convertIndividualTime(endTime)}`;
}

export default TimeConverter;