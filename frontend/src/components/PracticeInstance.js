import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import StreetView from './StreetViewComponent';
import Highlighter from "react-highlight-words";




function PracticeInstance({practice, searchQuery}) {
  // console.log(practice);
const imgStyle={
    height: '18rem',
    objectFit: 'cover',
    width: '100%',
    };
const cardStyle={
    height: '35rem',
    width: '100%',
    alignItems: 'center',
}

  const buttonStyle = {
    marginBottom: '1rem',
    marginLeft: '0.5rem',
    marginRight: '0.5rem',
    // backgroundColor: '#d01664',
    // borderColor: '#d01664',
  }

  const imageString = (str) => {
    return str.split(/\s+/).map(word => word.toLowerCase()).join('-');
  };

  let location = practice.city;
  if (practice.state) {
    location += ", " + practice.state;
  }
  let searchArray = searchQuery ? searchQuery.split(" ") : [];
  return (
    <Link to={`/practices/${practice.id}`} style={{ textDecoration: 'none' }}>
      <Card className="Card-lg" style={{display: 'flex', flex: '1', height: '100%'}}>
        {/* Uncomment this section if you have an image */}
        <Card.Img variant="top" src={practice.url} style={imgStyle} />
        <Card.Body>
          <Card.Title className="Card-text-dg" style={{ marginBottom: '5%' }}>
          <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={location}
          />
          </Card.Title>
          {/* <StreetView address={location} ht={'20vh'} zm={1}/> */}
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
          <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={`Number of Facilities: ${practice.num_facilities.toLocaleString()}`}
          />
          </Card.Text>
          {/* Uncomment this section if you have an address */}
          {/* <Card.Text>
              {practice.address}<br/>
              {practice.city}, {practice.state}, {practice.zipcode}<br/>
          </Card.Text> */}
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
          <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={`Women of Reproductive Age: ${practice.total_females.toLocaleString()}`}
          />
          </Card.Text>
          <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
          <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={`Average Distance/Time: ${practice.travel_distance.toLocaleString()} miles, ${practice.travel_time.toLocaleString()} mins`}
          />
            
          </Card.Text>
        </Card.Body>
      </Card>
    </Link>
  );
};

export default PracticeInstance;