import React, { useState, useRef, useEffect } from 'react';
import { Navbar, Nav, Container, Dropdown } from 'react-bootstrap';
import SearchBar from './SearchBar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

function NBar() {
    const [showSearch, setShowSearch] = useState(false);
    const searchRef = useRef(null);

    const toggleSearch = () => {
        setShowSearch(!showSearch);
    };

    // Handle click outside event
    const handleClickOutside = (event) => {
        if (searchRef.current && !searchRef.current.contains(event.target)) {
            setShowSearch(false);
        }
    };

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    return (
        <Navbar bg="dark" variant="dark" expand="lg" sticky="top" role="navigation">
            <Container fluid>
                <Navbar.Brand href="/" className="Header-style-NBar ml-auto" style={{ fontSize: '24px' }}>ChoiceConnect</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto Paragraph-style-dg-NBar align-items-center"> {/* Add align-items-center class to center vertically */}
                        <Nav.Link className="nav-link" href="/about">About</Nav.Link>
                        <Nav.Link className="nav-link" href="/community">Community Activities</Nav.Link>
                        <Nav.Link className="nav-link" href="/legislation">Legislation Information</Nav.Link>
                        <Nav.Link className="nav-link" href="/practices">Cities</Nav.Link>
                        <Dropdown>
                            <Dropdown.Toggle className="nav-link" variant="dark">
                                Visualizations
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item href="/visualizations">Our Visualizations</Dropdown.Item>
                                <Dropdown.Item href="/prov-visualizations">Provider Visualizations</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        <Nav.Link className="nav-link" href="/critiques">Critiques</Nav.Link>
                        <div style={{ marginRight: '20px' }}></div> {/* Add more space */}
                        <SearchBar onSearch={() => {}} global={true} style={{ minWidth: '30vw', maxWidth: '400px', backgroundColor: '#343a40', border: 'none', padding: '0.5rem 1rem' }}/>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NBar;