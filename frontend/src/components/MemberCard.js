import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import 'bootstrap/dist/css/bootstrap.css';

function MemberCard({ name, img, bio, role, commits = 0, issues = 0, tests = 0 }) {
  const [cardHeight, setCardHeight] = useState('auto');

  useEffect(() => {
    const updateCardHeight = () => {
      // Find the maximum height among all member cards
      const cards = document.getElementsByClassName('MemberCard');
      let maxHeight = 0;
      for (let i = 0; i < cards.length; i++) {
        maxHeight = Math.max(maxHeight, cards[i].clientHeight);
      }
      // Update the state with the maximum height
      setCardHeight(`${maxHeight}px`);
    };

    // Call the function initially and whenever the component updates
    updateCardHeight();
    window.addEventListener('resize', updateCardHeight);
    return () => {
      window.removeEventListener('resize', updateCardHeight);
    };
  }, [name, bio, role, commits, issues, tests]);

  const handleImageLoad = () => {
    // When the image loads, trigger an update to recalculate the card height
    setCardHeight('auto');
  };

  return (
      <Card className="Card MemberCard" style={{ width: '18rem', height: cardHeight }}>
        <Card.Img variant="top" src={img} style={{ objectFit: 'cover', height: '100%' }} onLoad={handleImageLoad} />
        <Card.Body>
          <Card.Title className="Card-title-lp">{name}</Card.Title>
          <Card.Text className="Card-text-lp" style={{ fontSize: '0.9rem' }}>
            {"Bio: " + bio}
          </Card.Text>
          <Card.Text className="Card-text-lp" style={{ fontSize: '0.9rem' }}>
            {"Role: " + role}
          </Card.Text>
          <Card.Text className="Card-text-lp" style={{ fontSize: '0.9rem' }}>
            Number of Commits: {commits}
          </Card.Text>
          <Card.Text className="Card-text-lp" style={{ fontSize: '0.9rem' }}>
            Number of Issues: {issues}
          </Card.Text>
          <Card.Text className="Card-text-lp" style={{ fontSize: '0.9rem' }}>
            Number of Unit Tests: {tests}
          </Card.Text>
        </Card.Body>
      </Card>
  );
}

export default MemberCard;