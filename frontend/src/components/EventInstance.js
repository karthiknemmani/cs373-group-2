import React from 'react';
import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import StreetView from './StreetViewComponent';
import TimeConverter from './ConvertTime';
import ConvertDate from './ConvertDate';
import Highlighter from "react-highlight-words";

function EventInstance({ event, currentDate, searchQuery}) {
  const isEventPassed = (eventDate) => {
    // console.log("Event Date: " + eventDate);
    // console.log("Current Date: " + currentDate);
    // console.log(new Date(eventDate) < new Date(currentDate));
    return new Date(eventDate) < new Date(currentDate);
  };
  const imgStyle={
    height: '18rem',
    objectFit: 'cover',
    width: '100%',
    };
  let location = event.venue + ',' + event.address;
  let searchArray = searchQuery ? searchQuery.split(" ") : [];
  return (
    <>
      {isEventPassed(event.date) ? (
        <Link to={`/community/${event.id}`} style={{ textDecoration: 'none' }}>
          <Card className="Card-lg" style={{ display: 'flex', flex: '1', height: '100%', opacity: '0.6' }}>
          <Card.Img variant="top" src={event.url} style={imgStyle} />
            <Card.Body>
            {"Event has passed"}
              <Card.Title className="Card-text-lp text-muted" style={{ marginBottom: '5%' }}>
                <Highlighter
                  searchWords={searchArray}
                  autoEscape={true}
                  textToHighlight={event.title}
                />
              </Card.Title>
              <Card.Subtitle className="Card-text-lp mb-2 text-muted">
                <Highlighter
                  searchWords={searchArray}
                  autoEscape={true}
                  textToHighlight={ConvertDate(event.date)}
                />     
              </Card.Subtitle>
              <Card.Text className="Card-text-lp text-muted" style={{ fontSize: '12px' }}>
                <Highlighter
                  searchWords={searchArray}
                  autoEscape={true}
                  textToHighlight={event.address}
                /><br />
                <Highlighter
                  searchWords={searchArray}
                  autoEscape={true}
                  textToHighlight={TimeConverter(event.time)}
                /><br />
              </Card.Text>
            </Card.Body>
          </Card>
        </Link>
      ) : (
        <Link to={`/community/${event.id}`} style={{ textDecoration: 'none' }}>
          <Card className="Card-lg" style={{display: 'flex', flex: '1', height: '100%'}}>
          <Card.Img variant="top" src={event.url} style={imgStyle} />
            <Card.Body>
              <Card.Title className="Card-text-dg" style={{ marginBottom: '5%' }}>              
              <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={event.title}
              /></Card.Title>
              <Card.Subtitle className="Card-text-dg mb-2 text-muted">
              <Highlighter
            searchWords={searchArray}
            autoEscape={true}
            textToHighlight={ConvertDate(event.date)}
          /> 
              </Card.Subtitle>
              {/* <Card.Subtitle className="Card-text-dg mb-2 text-muted">{event.date}</Card.Subtitle> */}
              {/* <StreetView address={location} ht={'20vh'} zm={1}/> */}
              <Card.Text className="Card-text-dg" style={{ fontSize: '12px' }}>
              <Highlighter
                searchWords={searchArray}
                autoEscape={true}
                textToHighlight={event.address}
              /><br />
              <Highlighter
                searchWords={searchArray}
                autoEscape={true}
                textToHighlight={TimeConverter(event.time)}
              />
              </Card.Text>
            </Card.Body>
          </Card>
        </Link>
      )}
    </>
  );
}

export default EventInstance;