import React, { useEffect, useState } from 'react';
import { GoogleMap, useLoadScript, MarkerF } from '@react-google-maps/api';
import {
    setKey,
    setDefaults,
    fromAddress,
  } from "react-geocode";
const libraries = ['places'];


const API_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

const MapContainer = ({address, ht='50vh', zm}) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: API_KEY,
  });
  const mapContainerStyle = {
    width: '100%',
    height: ht,
    alignItems: 'center',
  };  

    const [lat, setLat] = useState(null);
    const [lng, setLng] = useState(null);
    
  useEffect(() => {
    setDefaults({
        key: API_KEY,
        language: "en",
      });
    fromAddress(`${address}`)
    .then(({ results }) => {
      console.log("These are the longitude and latitude coordinates:");
      console.log(results[0].geometry.location);
      setLat(results[0].geometry.location.lat);
      setLng(results[0].geometry.location.lng);
    })
    .catch(console.error);
  }
, [address]);

  if (loadError) {
    return <div>Error loading maps</div>;
  }

  if (!isLoaded) {
    return <div>Loading maps</div>;
  }

  return (
    <div>
      <GoogleMap
        mapContainerStyle={mapContainerStyle}
        zoom={zm}
        center={{ lat: lat, lng: lng }}
      >
        <MarkerF position={{ lat: lat, lng: lng }} />
      </GoogleMap>
    </div>
  );
};

export default MapContainer;
