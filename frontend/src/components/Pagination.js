import React from 'react';
import ReactPaginate from 'react-paginate';

const Pagination = ({currentPage, totalPages, handlePageChange }) => {
  return (
    <ReactPaginate
      pageCount={totalPages}
      pageRangeDisplayed={3}
      marginPagesDisplayed={1}
      onPageChange={handlePageChange}
      forcePage={currentPage}
      containerClassName={'pagination'}
      activeClassName={'active'}
      breakClassName={'page-item'}
      pageClassName={'page-item'}
      breakLinkClassName={'page-link'}
      pageLinkClassName={'page-link'}
      previousClassName={'page-item'}
      nextClassName={'page-item'}
      previousLinkClassName={'page-link'}
      nextLinkClassName={`page-link ${totalPages <= 1 ? 'disabled' : ''}`}
      previousLabel={'Prev'}
    />
  );
};

export default Pagination;